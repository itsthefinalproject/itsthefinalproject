﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CLayoutPreferencesVM
// Abstract : ViewModel for the layout preferences view
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicSEC.Models;
using MusicSEC.DAL;
using MusicSEC.Utility;

namespace MusicSEC.ViewModels
{
    public class COperationsLengthVM
    {
        public int CBusinessSettingsID { get; set; }
        public int COperationsLengthID { get; set; }
        public int intStartHours { get; set; }
        public int intStartMins { get; set; }
        public int intEndHours { get; set; }
        public int intEndMins { get; set; }
        public string strDayName { get; set; }
        public int intDayCode { get; set; }
        public int intStartAm { get; set; }
        public int intEndAm { get; set; }
        public bool blnIsOperational { get; set; }

        //-------------------------------------------------------------------------
        //  Name: MapViewModel
        //  Abstract: Map a view model for the operation length display
        //-------------------------------------------------------------------------
        public COperationsLengthVM MapViewModel(COperationsLength oplength)
        {
            this.CBusinessSettingsID = oplength.CBusinessSettingsID;
            this.COperationsLengthID = oplength.COperationsLengthID;
            this.intStartHours = CTimeUtility.ConvertHours(oplength.intStartTime);
            this.intStartMins = CTimeUtility.ConvertMinutes(oplength.intStartTime);
            
            this.intEndHours = CTimeUtility.ConvertHours(oplength.intEndTime);
            this.intEndMins = CTimeUtility.ConvertMinutes(oplength.intEndTime);
            this.intDayCode = oplength.intDayCode;
            this.intStartAm = CTimeUtility.IsAm(oplength.intStartTime);
            this.intEndAm = CTimeUtility.IsAm(oplength.intEndTime);
            this.blnIsOperational = oplength.blnIsOperational;

            return this;
        }

        //-------------------------------------------------------------------------
        //  Name: Map model
        //  Abstract: Map model from viewmodel
        //-------------------------------------------------------------------------
        public COperationsLength MapModel(COperationsLengthVM vm, CUnitOfWork unit)
        {
            COperationsLength opLength = unit.OpLengthRep.FindById(vm.COperationsLengthID);
            bool blnStartAm = (vm.intStartAm == 0) ? true : false;
            bool blnEndAm = (vm.intEndAm == 0) ? true : false;
            opLength.CBusinessSettingsID = CBusinessSettingsID;
            opLength.intStartTime = CTimeUtility.MinutesSinceMidnight(vm.intStartHours, vm.intStartMins, blnStartAm);
            opLength.intEndTime = CTimeUtility.MinutesSinceMidnight(vm.intEndHours, vm.intEndMins, blnEndAm);
            opLength.intDayCode = vm.intDayCode;
            opLength.blnIsOperational = vm.blnIsOperational;
            return opLength;
            
        }
    }
}