﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CCalendarVM
// Abstract : ViewModel for the calendar view.
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using MusicSEC.DAL;


namespace MusicSEC.ViewModels
{
    public class CCalendarVM
    {
        public string strMonthName { get; set; }
        public List<CDayVM> lstDays { get; set; }

        private int intDaysInMonth;

        //Constructor
        public CCalendarVM()
        { 
        }

        //Constructor with overoads
        public CCalendarVM(int monthIndex, int yearIndex, CUnitOfWork unit)
        {
            lstDays = new List<CDayVM>();
            strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(monthIndex);
            intDaysInMonth = DateTime.DaysInMonth(yearIndex, monthIndex);
            DateTime dtmDate = new DateTime(yearIndex, monthIndex, 1);
            int intDayOne = dtmDate.Day;
            for (int i = intDayOne; i < intDayOne + intDaysInMonth; i++)
            {
                var day = unit.DayRep.GetAll(x => x.dtmDate.Month == monthIndex && x.dtmDate.Year == yearIndex && x.dtmDate.Day == i).FirstOrDefault();
                lstDays.Add(new CDayVM().MapViewModel(day, unit));
            }

        }
    }
}