﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CStudentDash
// Abstract : ViewModel for the Student Dash
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicSEC.Models;

namespace MusicSEC.ViewModels
{
    public class CStudentDashVM
    {
        public CStudent DashStudent { get; set; }
        public CInstructor DashInstructor { get; set; }
        public List<CLesson> lstUpcomingLessons { get; set; }
    }
}