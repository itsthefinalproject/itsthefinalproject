﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CUserContentVM
// Abstract : ViewModel for the user content
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicSEC.ViewModels
{
    public class CUserContentVM
    {
        public string strHtml { get; set; }
        public string strContainerId { get; set; }
    }
}