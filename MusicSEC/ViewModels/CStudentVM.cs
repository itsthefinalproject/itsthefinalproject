﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CStudentVm
// Abstract : ViewModel for the student view
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicSEC.DAL;
using MusicSEC.Models;
using System.ComponentModel.DataAnnotations;

namespace MusicSEC.ViewModels
{
    public class CStudentVM
    {
        public int CStudentID { get; set; }
        public int CInstructorID { get; set; }
        
        [Required(ErrorMessage = "A username is required")]
        public string strUserName { get; set; } // Used on creation of student
        
        [Required(ErrorMessage = "A password is required")]
        public string strPassword { get; set; } // Used on creation of student

        [Required(ErrorMessage = "A first name is required")]
        public string strFirstName { get; set; }

        [Required(ErrorMessage = "A last name is required")]
        public string strLastName { get; set; }

        public string strParentsFirstName { get; set; }
  
        [Required(ErrorMessage = "An email address is required")]
        public string strEmailAddress { get; set; }

        [Required(ErrorMessage = "A phone number is required")]
        public string strPhoneNumber { get; set; }

        [Required(ErrorMessage = "An address is required")]
        public string strAddress { get; set; }

        [Required(ErrorMessage = "A city is required")]
        public string strCity { get; set; }

        [Required(ErrorMessage = "A state is required")]
        public string strState { get; set; }

        [Required(ErrorMessage = "A zip code is required")]
        public string strZipCode { get; set; }

        public string strInstructorName { get; set; }
        public bool blnActive { get; set; }
        public bool blnFirstTime { get; set; }

        public CUserImage IntstructorImage { get; set; }
       
        // Property return full name of student
        public string FullName
        {
            get { return strFirstName + " " + strLastName; }
        }


        //-------------------------------------------------------------------------
        //  Name: MapViewModel
        //  Abstract: Map a view model from a student model
        //-------------------------------------------------------------------------
        public CStudentVM MapViewModel(CUnitOfWork unit, CStudent student)
        {
            this.CStudentID = student.CStudentID;
            this.CInstructorID = student.CInstructorID;
            this.strFirstName = student.strFirstName;
            this.strLastName = student.strLastName;
            this.strParentsFirstName = student.strParentsFirstName;
            this.strPhoneNumber = student.strPhoneNumber;
            this.strEmailAddress = student.strEmailAddress;
            this.strAddress = student.strAddress;
            this.strCity = student.strCity;
            this.strState = unit.StateRep.GetAll(x => x.CStateID == student.CStateID).Select(x => x.strStateName).FirstOrDefault();
            this.strZipCode = student.strZipCode;
            this.blnActive = student.blnActive;
            this.blnFirstTime = student.blnFirstTime;
            this.strUserName = unit.UsersRep.GetAll(x => x.UserId == student.intUserID).Select(x => x.UserName).FirstOrDefault();

            if (CInstructorID != 0)
            {
                var instructor = new CInstructorVM().MapViewModel(unit, this.CInstructorID, true);
                this.strInstructorName = instructor.FullName;
                this.IntstructorImage =  instructor.ProfileImage;
            }
            return this;
        }

        //-------------------------------------------------------------------------
        //  Name: Map Model
        //  Abstract: map model from viewmodel
        //-------------------------------------------------------------------------
        public CStudent MapModel(CUnitOfWork unit, CStudentVM viewModel)
        {
            var student = unit.StudentRep.FindById(viewModel.CStudentID);

            if (student == null)
                student = new CStudent();

            student.CInstructorID = viewModel.CInstructorID;
            student.strFirstName = viewModel.strFirstName;
            student.strLastName = viewModel.strLastName;
            student.strParentsFirstName = viewModel.strParentsFirstName;
            student.strEmailAddress = viewModel.strEmailAddress;
            student.strPhoneNumber = viewModel.strPhoneNumber;
            student.strAddress = viewModel.strAddress;
            student.strCity = viewModel.strCity;
            student.CStateID = unit.StateRep.GetAll(x => x.strStateName.Trim().ToUpper() == viewModel.strState.Trim().ToUpper()).
                                             Select(x => x.CStateID).FirstOrDefault();
            student.strZipCode = viewModel.strZipCode;
            student.blnActive = viewModel.blnActive;
            student.blnFirstTime = viewModel.blnFirstTime;

            return student;
        }

    }
}