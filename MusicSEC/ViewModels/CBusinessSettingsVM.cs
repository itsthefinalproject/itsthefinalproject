﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CBusinessSettingsVM
// Abstract : ViewModel for the Business Settings View
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicSEC.Models;
using MusicSEC.DAL;

namespace MusicSEC.ViewModels
{
    public class CBusinessSettingsVM
    {
        public int CBusinessSettingsID { get; set; }
        public string strBusinessName { get; set; }
        public string strTermsOfAgreement { get; set; }
        public int intLessonLengthInMins { get; set; }
        public List<COperationsLengthVM> lstOperationsLengths { get; set; } // size 7 for the days of the week
        private string[] astrDay;
        
        
        public CBusinessSettingsVM()
        {
            lstOperationsLengths = new List<COperationsLengthVM>(7);
            astrDay = new string[7] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
        }

        //-------------------------------------------------------------------------
        //  Name: MapModel
        //  Abstract: Map the Business Settings Viewmodel back to a Business Settings model
        //-------------------------------------------------------------------------
        public CBusinessSettings MapModel(CBusinessSettingsVM vm, CUnitOfWork unit)
        {
            var businessSettings = unit.BusinessSettingsRep.FindById(vm.CBusinessSettingsID);
            businessSettings.strBusinessName = vm.strBusinessName;
            businessSettings.strTermsOfAgreement = vm.strTermsOfAgreement;
            businessSettings.intLessonLengthInMins = vm.intLessonLengthInMins;

            for (int i = 0; i < vm.lstOperationsLengths.Count; i++)
            {
                int intDayIndex = vm.lstOperationsLengths[i].intDayCode;
                var oplength = unit.OpLengthRep.FindById(vm.lstOperationsLengths[i].COperationsLengthID);
                oplength = vm.lstOperationsLengths[i].MapModel(vm.lstOperationsLengths[i], unit);
            }
            return businessSettings;
            
        }

        //-------------------------------------------------------------------------
        //  Name: MapViewModel
        //  Abstract: Map Business Settings Viewmodel from the database record
        //-------------------------------------------------------------------------
        public CBusinessSettingsVM MapViewModel(CUnitOfWork unit)
        {
            var businessSettings = unit.BusinessSettingsRep.GetAll().FirstOrDefault();
            this.CBusinessSettingsID = businessSettings.CBusinessSettingsID;
            this.strBusinessName = businessSettings.strBusinessName;
            this.intLessonLengthInMins = businessSettings.intLessonLengthInMins;
            this.strTermsOfAgreement = businessSettings.strTermsOfAgreement;

            foreach (var oplengths in businessSettings.OperationsLength)
            {
                var operationsLengthVM = new COperationsLengthVM().MapViewModel(oplengths);
                int intDayIndex = operationsLengthVM.intDayCode;
                operationsLengthVM.strDayName = astrDay[intDayIndex];
                this.lstOperationsLengths.Add(operationsLengthVM);
                               
            }

            return this;                
        }

        
    }
}