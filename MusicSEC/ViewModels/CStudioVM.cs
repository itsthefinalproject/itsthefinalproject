﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CStudioVM
// Abstract : ViewModel for the studio view
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicSEC.DAL;
using MusicSEC.Models;
using MusicSEC.ViewModels;

namespace MusicSEC.ViewModels
{
    public class CStudioVM
    {
        public int CStudioID { get; set; }
        public int CDayID { get; set; }
        public string strStudioName { get; set; }
        public int intLessonSlotsPerday { get; set; }
        // int is lesson slot index there will be intLessonSlotsPerDay
        public Dictionary<int, CLessonVM> dctLessonSlots { get; set; }

        //-------------------------------------------------------------------------
        //  Name: MapViewModel
        //  Abstract: Map a view model from the studio model
        //-------------------------------------------------------------------------
        public CStudioVM MapViewModel(CStudio studio, CUnitOfWork unit)
        {
            var businessSettings = unit.BusinessSettingsRep.GetAll().FirstOrDefault();
            this.CStudioID = studio.CStudioID;
            this.CDayID = studio.CDayID;
            this.strStudioName = unit.StudioNameRep.FindById(studio.CStudioNameID).strStudioName;
            this.intLessonSlotsPerday = studio.intLessonSlotsPerDay;
            this.dctLessonSlots = InitLessonSlots(studio, businessSettings, unit);
            
            return this;

        }

        //-------------------------------------------------------------------------
        //  Name: InitLessonSlots
        //  Abstract: determine the number of lessons that will operate based on the 
        //            length of the business day / length of a lesson
        //-------------------------------------------------------------------------
        private Dictionary<int, CLessonVM> InitLessonSlots(CStudio studio, CBusinessSettings settings, CUnitOfWork unit)
        {
            dctLessonSlots = new Dictionary<int, CLessonVM>();
            for (int i = 0; i < intLessonSlotsPerday; i++)
            {
                CLessonVM lessonVM = new CLessonVM();
                lessonVM.CStudioID = studio.CStudioID;
                lessonVM.blnAvailable = true;
                lessonVM.intStartTime = lessonVM.CalculateStartTime(settings.intLessonLengthInMins, i, unit.DayRep.FindById(studio.CDayID).intOperationsStartTime);
                lessonVM.intEndTime = lessonVM.intStartTime + settings.intLessonLengthInMins;
                if (unit.LessonRep.GetAll().Any(x => x.CStudioID == studio.CStudioID && x.intStartTime == lessonVM.intStartTime))
                {
                    lessonVM = lessonVM.MapViewModel(unit.LessonRep.GetAll(x => x.CStudioID == studio.CStudioID && x.intStartTime == lessonVM.intStartTime).FirstOrDefault(), unit);
                    lessonVM.blnAvailable = false;
                }
                dctLessonSlots.Add(i, lessonVM);
            }

            return dctLessonSlots;
        }
    }
}