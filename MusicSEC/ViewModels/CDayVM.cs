﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CDayVM
// Abstract : ViewModel for the day view
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using MusicSEC.DAL;
using MusicSEC.Models;
using MusicSEC.Utility;

namespace MusicSEC.ViewModels
{
    public class CDayVM
    {
        public int CDayID { get; set; }
        public DateTime dtmDate { get; set; }
        public int intDay { get; set; }
        public int intMonth { get; set; }
        public int intYear { get; set; }
        public int intStartTime { get; set; }
        public int intEndTime { get; set; }
        public int intLessonsPerDay { get; set; }
        public bool blnOperational { get; set; }
        public List<CStudioVM> lstStudios { get; set; }
        public COperationsLengthVM OplengthVm { get; set; }

        //Constructor
        public CDayVM()
        {
            lstStudios = new List<CStudioVM>();
        }

        //Property to return the day name
        public string DayName
        {
            get { return new DateTime(intYear, intMonth, intDay).DayOfWeek.ToString(); }
        }

        //Property to return the month Name
        public string MonthName
        {
            get { return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(intMonth); }
        }

        //-------------------------------------------------------------------------
        //  Name: MapViewModel
        //  Abstract: Map a view model for the Day View
        //-------------------------------------------------------------------------
        public CDayVM MapViewModel(CDay day, CUnitOfWork unit)
        {
            if (day != null)
            {
                this.CDayID = day.CDayID;
                this.dtmDate = day.dtmDate;
                this.intDay = day.dtmDate.Day;
                this.intMonth = day.dtmDate.Month;
                this.intYear = day.dtmDate.Year;
                this.intLessonsPerDay = day.Studios.Select(x => x.intLessonSlotsPerDay).FirstOrDefault();
                this.intStartTime = day.intOperationsStartTime;
                this.intEndTime = day.intOperationsEndTime;
               
                
                this.blnOperational = day.blnOperational;
                this.OplengthVm = new COperationsLengthVM();

                OplengthVm.strDayName = this.DayName;

                OplengthVm.intStartMins = CTimeUtility.ConvertMinutes(day.intOperationsStartTime);
                OplengthVm.intStartHours = CTimeUtility.ConvertHours(day.intOperationsStartTime);
                OplengthVm.intStartAm = CTimeUtility.IsAm(day.intOperationsStartTime);

                OplengthVm.intEndMins = CTimeUtility.ConvertMinutes(day.intOperationsEndTime);
                OplengthVm.intEndHours = CTimeUtility.ConvertHours(day.intOperationsEndTime);
                OplengthVm.intEndAm = CTimeUtility.IsAm(day.intOperationsEndTime);

                OplengthVm.blnIsOperational = day.blnOperational;
                

                foreach (var studio in day.Studios.ToList())
                {
                    var studioVM = new CStudioVM().MapViewModel(studio, unit);
                    this.lstStudios.Add(studioVM);
                }
            }
            return this;
        }

        //-------------------------------------------------------------------------
        //  Name: MapModel
        //  Abstract: Map day view model back to day model
        //-------------------------------------------------------------------------
        public CDay MapModel(CDayVM dayVM, CUnitOfWork unit)
        {
            var day = unit.DayRep.FindById(dayVM.CDayID);
            if (day.Studios.Select(x => x.intLessonSlotsPerDay).FirstOrDefault() != dayVM.intLessonsPerDay)
            {
                foreach (var studio in day.Studios.ToList())
                {
                    studio.intLessonSlotsPerDay = dayVM.intLessonsPerDay;
                }
            }
            day.intOperationsStartTime = dayVM.intStartTime;
            day.intOperationsEndTime = dayVM.intEndTime;
            day.blnOperational = dayVM.blnOperational;

            return day;
        }
    }
}