﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CLessonVM
// Abstract : ViewModel for the Lessons view
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicSEC.DAL;
using MusicSEC.Models;
using MusicSEC.Utility;

namespace MusicSEC.ViewModels
{
    public class CLessonVM
    {
        public int CLessonID { get; set; }
        public int CStudentID { get; set; }
        public int CInstructorID { get; set; }
        public int CStudioID { get; set; }
        public int intStartTime { get; set; }
        public int intEndTime { get; set; }
        public int intStatusId { get; set; }
        public bool blnAvailable { get; set; }
        public string strComments { get; set; }
        public string strStudentName { get; set; }
        public string strInstructorName { get; set; }
        public string strStudioName { get; set; }
        public DateTime dtmDate { get; set; }
        private LessonStatus eStatus;

        // Constructor
        public CLessonVM()
        { 
        }

        //-------------------------------------------------------------------------
        //  Name: CalculateStartTime
        //  Abstract: Calculate the start time of a lesson 
        //-------------------------------------------------------------------------
        public int CalculateStartTime(int lessonLength, int lessonIndex, int operationStartTime)
        {
            return operationStartTime + (lessonLength * lessonIndex);
        }

        //-------------------------------------------------------------------------
        //  Name: ConvertStartTime
        //  Abstract: Return start time as a readable string instead of minutes since midnight
        //-------------------------------------------------------------------------
        public string ConvertStartTime
        {
            get { return CTimeUtility.ConvertToReadable(intStartTime); }
        }

        //-------------------------------------------------------------------------
        //  Name: InstructorFullName
        //  Abstract: return the instructors full name
        //-------------------------------------------------------------------------
        public string InstructorFullName(CUnitOfWork unit)
        {
            return unit.InstructorRep.GetAll(x => x.CInstructorID == this.CInstructorID).Select(x => x.strFirstName + " " + x.strLastName).FirstOrDefault().ToString() ?? "";
        }

        //-------------------------------------------------------------------------
        //  Name: LessonDate
        //  Abstract: Return the date of the lesson
        //-------------------------------------------------------------------------
        public DateTime LessonDate(CUnitOfWork unit)
        {
            int intDayId = unit.StudioRep.GetAll(x => x.CStudioID == this.CStudioID).Select(x => x.CDayID).FirstOrDefault();
            return unit.DayRep.GetAll(x => x.CDayID == intDayId).Select(x => x.dtmDate).FirstOrDefault();
        }

        //-------------------------------------------------------------------------
        //  Name: StudentFullName
        //  Abstract: Return the students first name
        //-------------------------------------------------------------------------
        public string StudentFullName(CUnitOfWork unit)
        {
            return unit.StudentRep.GetAll(x => x.CStudentID == this.CStudentID).Select(x => x.strFirstName + " " + x.strLastName).FirstOrDefault().ToString() ?? "";
        }

        //-------------------------------------------------------------------------
        //  Name: StudioName
        //  Abstract: Return the studio name
        //-------------------------------------------------------------------------
        public string StudioName(CUnitOfWork unit)
        {
            int intStudioNameId = unit.StudioRep.GetAll(x => x.CStudioID == this.CStudioID).Select(x => x.CStudioNameID).FirstOrDefault();
            return unit.StudioNameRep.GetAll(x => x.CStudioNameID == intStudioNameId).Select(x => x.strStudioName).FirstOrDefault();
        }

        // Property return status id
        public int GetStatusId
        {
            get { return (int)this.eStatus; }
        }

        // Property Return Status as string
        public string GetStatus
        {
            get
            {
                string strStatus = eStatus.ToString();
                return strStatus.Replace("_", " ");
            }
        }

        //-------------------------------------------------------------------------
        //  Name: MapViewModel
        //  Abstract: Map a view model from a lesson model
        //-------------------------------------------------------------------------
        public CLessonVM MapViewModel(CLesson lesson, CUnitOfWork unit)
        {
            this.CLessonID = lesson.CLessonID;
            this.CStudentID = lesson.CStudentID;
            this.CInstructorID = lesson.CInstructorID;
            this.intStartTime = lesson.intStartTime;
            this.intEndTime = lesson.intEndTime;         
            this.strComments = lesson.strInternalComments;
            this.CStudioID = lesson.CStudioID;
            this.blnAvailable = false;
            this.strInstructorName = InstructorFullName(unit);
            this.strStudentName = StudentFullName(unit);
            this.strStudioName = StudioName(unit);
            this.dtmDate = LessonDate(unit);
            SetStatus(lesson.intStatus, this.dtmDate);

            return this;
        }

        //-------------------------------------------------------------------------
        //  Name: MapModel
        //  Abstract: Map model from viewmodel
        //-------------------------------------------------------------------------
        public CLesson MapModel(CLessonVM lessonVm, CUnitOfWork unit)
        {
            var lesson = unit.LessonRep.FindById(lessonVm.CLessonID);
            if (lesson == null)
                lesson = new CLesson();

            lesson.CInstructorID = lessonVm.CInstructorID;
            lesson.CStudentID = lessonVm.CStudentID;
            lesson.CStudioID = lessonVm.CStudioID;
            lesson.intStartTime = lessonVm.intStartTime;
            lesson.intEndTime = lessonVm.intEndTime;
            lesson.intStatus = GetStatusId;
            lesson.strInternalComments = lessonVm.strComments;

            return lesson;
        }

        //-------------------------------------------------------------------------
        //  Name: SetStatus
        //  Abstract: set the lessons status if it has not been completed
        //-------------------------------------------------------------------------
        private void SetStatus(int statusId, DateTime date)
        {
            this.eStatus = (LessonStatus)statusId;
            if(this.eStatus != LessonStatus.Canceled && date < DateTime.Now) 
            {
                this.eStatus = LessonStatus.Needs_Makeup;
                this.intStatusId = 3;
            } 
        }
    }
}