﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : AcademyStatVM
// Abstract : ViewModel for the academy statistics view
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicSEC.DAL;

namespace MusicSEC.ViewModels
{
    public class AcademyStatVM
    {
        public int intActiveInstructorCnt { get; set; }
        public int intInActiveInstructorCnt { get; set; }
        public int intActiveStudentCnt { get; set; }
        public int intInActiveStudentCnt { get; set; }
        public string strMostUsedStudio { get; set; }
        public string strLeastUsedStudio { get; set; }
        public bool blnValid { get; set; }
        public DateTime dtmStartDate { get; set; }
        public DateTime dtmEndDate { get; set; }
        public List<CLessonVM> lstLessons { get; set; }
        public Dictionary<string, AcademyStatVMStudioHelper> DctStudioHelpers;
        private List<AcademyStatVMHelper> lstHelpers;

        // Constructor
        public AcademyStatVM()
        {
            blnValid = false;
        }
        // Constructor with overloads
        public AcademyStatVM(DateTime start, DateTime end)
        {
            dtmStartDate = start;
            dtmEndDate = end;
            CheckDates();
            blnValid = false;
        }

        //-------------------------------------------------------------------------
        //  Name: MapViewModel
        //  Abstract: Map a view model for the Academy Stat View
        //-------------------------------------------------------------------------
        public AcademyStatVM MapViewModel(CUnitOfWork unit)
        {
            this.intActiveInstructorCnt = unit.InstructorRep.GetAll(x => x.blnActive == true).Count();
            this.intInActiveInstructorCnt = unit.InstructorRep.GetAll(x => x.blnActive == false).Count();
            this.intActiveStudentCnt = unit.StudentRep.GetAll(x => x.blnActive == true).Count();
            this.intInActiveStudentCnt = unit.StudentRep.GetAll(x => x.blnActive == false).Count();
            this.strMostUsedStudio = MostUsedStudio(unit);
            this.strLeastUsedStudio = LeastUsedStudio(unit);
            this.lstLessons = AllLesssons(unit);
            this.DctStudioHelpers = InitDict(unit);
            this.blnValid = true;

            return this;
        }

        //-------------------------------------------------------------------------
        //  Name: InitHelperList
        //  Abstract: Get a list of studios for a given date range
        //-------------------------------------------------------------------------
        private void InitHelperList(CUnitOfWork unit)
        {
            var studioNames = unit.StudioNameRep.GetAll().ToList();
            lstHelpers = new List<AcademyStatVMHelper>();

            foreach (var studioName in studioNames)
            {
                var studios = unit.StudioRep.GetAll(x => x.CStudioNameID == studioName.CStudioNameID && 
                                                         x.Day.dtmDate >= dtmStartDate.Date && 
                                                         x.Day.dtmDate <= dtmEndDate.Date).
                                             ToList();
        
                foreach (var studio in studios)
                {
                    if (lstHelpers.Exists(x => x.intStudioNameId == studioName.CStudioNameID))
                    {
                        var helper = lstHelpers.FirstOrDefault(x => x.intStudioNameId == studioName.CStudioNameID);
                        helper.intLessonCount += studio.StudioLessons.Count;
                    }
                    else
                    {
                        var helper = new AcademyStatVMHelper(studioName.CStudioNameID, studio.StudioLessons.Count);
                        lstHelpers.Add(helper);
                    }
                }
            }
        }

        //-------------------------------------------------------------------------
        //  Name: MostUsedStudio
        //  Abstract: Find the studio that has had the most lessons
        //-------------------------------------------------------------------------
        private string MostUsedStudio(CUnitOfWork unit)
        {
            string strStudioName = "";

            if (lstHelpers == null || lstHelpers.Count == 0)
                InitHelperList(unit);

            if (lstHelpers.Count > 0)
            {
                var helper = lstHelpers[0];
                foreach (var helpers in lstHelpers)
                {
                    if (helper.intLessonCount < helpers.intLessonCount)
                        helper = helpers;
                }
                strStudioName = unit.StudioNameRep.FindById(helper.intStudioNameId).strStudioName;
            }
            return strStudioName;

        }

        //-------------------------------------------------------------------------
        //  Name: LeastUsedStudio
        //  Abstract: Find the studio with the least usage.
        //-------------------------------------------------------------------------
        private string LeastUsedStudio(CUnitOfWork unit)
        {
            string strStudioName = "";

            if (lstHelpers == null || lstHelpers.Count == 0)
                InitHelperList(unit);

            if (lstHelpers.Count > 0)
            {
                var helper = lstHelpers[0];
                foreach (var helpers in lstHelpers)
                {
                    if (helper.intLessonCount > helpers.intLessonCount)
                        helper = helpers;
                }
                strStudioName = unit.StudioNameRep.FindById(helper.intStudioNameId).strStudioName;
            }
            return strStudioName;
        }

         //-------------------------------------------------------------------------
        //  Name: AllLesssons
        //  Abstract: Return all lessons for Admin
        //-------------------------------------------------------------------------
        private List<CLessonVM> AllLesssons(CUnitOfWork unit)
        {
            var lessons = unit.LessonRep.GetAll().ToList();
            lstLessons = new List<CLessonVM>();
            foreach (var lesson in lessons)
            {
                if (lesson.Studio.Day.dtmDate.Date >= this.dtmStartDate && lesson.Studio.Day.dtmDate.Date <= this.dtmEndDate)
                {
                    lstLessons.Add(new CLessonVM().MapViewModel(lesson, unit));
                }
            }
            lstLessons = lstLessons.OrderBy(x => x.dtmDate).ThenBy(x => x.intStartTime).ToList();
            return lstLessons;
        }

        private Dictionary<string, AcademyStatVMStudioHelper> InitDict(CUnitOfWork unit)
        {
            DctStudioHelpers = new Dictionary<string, AcademyStatVMStudioHelper>();
            var lstStudios = unit.DayRep.GetAll(x => x.dtmDate >= this.dtmStartDate && x.dtmDate <= this.dtmEndDate && x.blnOperational == true).SelectMany(x => x.Studios).ToList();
            

            foreach (var lessonStudio in lstStudios)
            {
                int intLessonCnt = unit.LessonRep.GetAll(x => x.CStudioID == lessonStudio.CStudioID).Count();
                int totalLessons = lessonStudio.intLessonSlotsPerDay;
                string strStudioName = lessonStudio.StudioName.strStudioName;
                if (DctStudioHelpers.ContainsKey(strStudioName))
                {
                    DctStudioHelpers[strStudioName].intNumberOfLessons += intLessonCnt;
                    DctStudioHelpers[strStudioName].intTotalLessons += totalLessons;
                }
                else
                {
                    DctStudioHelpers.Add(strStudioName, new AcademyStatVMStudioHelper(intLessonCnt, totalLessons));
                }
            }


            return DctStudioHelpers;
        }

        private void CheckDates()
        {
            if (this.dtmStartDate.Date >= this.dtmEndDate.Date)
                this.dtmEndDate = this.dtmEndDate.AddDays(1);
        }
    }

    //---------------------------------------------------------------
    // Author : Adam Shaver
    // Class : AcademyStatVMHelper
    // Abstract : Helper class for the AcademyStatVM
    //---------------------------------------------------------------
    public class AcademyStatVMHelper
    {
        public int intStudioNameId { get; set; }
        public int intLessonCount { get; set; }

        public AcademyStatVMHelper()
        { 
        }

        public AcademyStatVMHelper(int studioNameId, int lessonCnt)
        {
            intStudioNameId = studioNameId;
            intLessonCount = lessonCnt;
        }
    }

    public class AcademyStatVMStudioHelper
    {
        public int intNumberOfLessons { get; set; }
        public int intTotalLessons { get; set; }
        public decimal dcmPercentUsed 
        {
            get { return (intTotalLessons == 0) ? 0 : (decimal)intNumberOfLessons / (decimal)intTotalLessons; }
            set { dcmPercentUsed = value; }
        }

        public AcademyStatVMStudioHelper()
        {

        }

        public AcademyStatVMStudioHelper(int numberlessons, int totallessons)
        {
           this.intNumberOfLessons = numberlessons;
           this.intTotalLessons = totallessons;
        }
    }
}