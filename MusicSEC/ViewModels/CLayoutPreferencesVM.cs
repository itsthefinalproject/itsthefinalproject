﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CLayoutPreferencesVM
// Abstract : ViewModel for the layout preferences view
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using MusicSEC.DAL;
using MusicSEC.Utility;
using MusicSEC.Models;

namespace MusicSEC.ViewModels
{
    public class CLayoutPreferencesVM
    {
        public int CUserLayoutPreferencesID { get; set; }
        public Dictionary<string, string> dctLayouts { get; set; }
        public string strBackgroundColor { get; set; }
        public string strFontColor { get; set; }
        public string strChosenLayout { get; set; }
        public string strImageName { get; set; }
        public int CUserImageID { get; set; }

        //Constructor
        public CLayoutPreferencesVM()
        {
            dctLayouts = CLayoutUtility.LayoutDict; 
        }

        //-------------------------------------------------------------------------
        //  Name: MapViewModel
        //  Abstract: Map a view model for the LayoutPreference View
        //-------------------------------------------------------------------------
        public CLayoutPreferencesVM MapViewModel(CUnitOfWork unit)
        {
            var layoutPrefs = unit.LayoutPrefsRep.GetAll().FirstOrDefault();
            if (layoutPrefs != null)
            {
                this.CUserLayoutPreferencesID = layoutPrefs.CUserLayoutPreferencesID;
                this.strBackgroundColor = layoutPrefs.strBackgroundColor;
                this.strFontColor = layoutPrefs.strFontColor;
                this.strChosenLayout = layoutPrefs.strLayout;
                this.CUserImageID = layoutPrefs.CUserImageID ?? 0;
                if (this.CUserImageID != 0)
                {
                    this.strImageName = unit.ImageRep.GetAll(x => x.CUserImageID == this.CUserImageID).Select(x => x.strFileType).FirstOrDefault();
                }
            }

            return this;                
        }

        //-------------------------------------------------------------------------
        //  Name: Map Model
        //  Abstract: Map model from viewmodel
        //-------------------------------------------------------------------------
        public CUserLayoutPreferences MapModel(CUnitOfWork unit, CLayoutPreferencesVM layoutVM)
        {
            var layoutPrefs = unit.LayoutPrefsRep.GetAll().FirstOrDefault();

            if (layoutPrefs == null)
            {
                layoutPrefs = new CUserLayoutPreferences();
            }

            layoutPrefs.strBackgroundColor = layoutVM.strBackgroundColor;
            layoutPrefs.strFontColor = layoutVM.strFontColor;
            layoutPrefs.strLayout = layoutVM.strChosenLayout;

            CLayoutUtility.CurrentLayout = layoutPrefs.strLayout;
            CLayoutUtility.BackgroundColor = layoutPrefs.strBackgroundColor;
            CLayoutUtility.FontColor = layoutPrefs.strFontColor;
            CLayoutUtility.SetBackgroundImgSrc = layoutVM.strImageName;

            if (layoutVM.CUserImageID == 0)
            {
                layoutPrefs.CUserImageID = null;
            }
            else
            {
                layoutPrefs.CUserImageID = layoutVM.CUserImageID;
            }
            
            return layoutPrefs;
        }

      
    }
}