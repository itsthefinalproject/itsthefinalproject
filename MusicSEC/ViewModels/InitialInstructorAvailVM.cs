﻿//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicSEC.DAL;
using MusicSEC.Models;

namespace MusicSEC.ViewModels
{
    //---------------------------------------------------------------
    // Author : Adam Shaver
    // Class : InitalInstructorAvailVMList
    // Abstract : List to hold InstructorAvailVM's
    //---------------------------------------------------------------
    public class InitialInstructorAvailVMList
    {
        public List<InitialInstructorAvailVM> lstAvailability { get; set; }
        private int intInstructorId;
        private string[] aDayNames = new string[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
        
        // Constructor
        public InitialInstructorAvailVMList()
        {
        }

        // Constructor with overloads
        public InitialInstructorAvailVMList(CUnitOfWork unit, int instructorId)
        {
            lstAvailability = new List<InitialInstructorAvailVM>(7);
            intInstructorId = instructorId;
            InitAvailabilityArray(unit);
        }

        //-------------------------------------------------------------------------
        //  Name: InitAvailabilityArray
        //  Abstract: Add 7 sets of instructor availability 
        //-------------------------------------------------------------------------
        private void InitAvailabilityArray(CUnitOfWork unit)
        {
            for (int i = 0; i < 7; i++)
            {
                lstAvailability.Add(new InitialInstructorAvailVM(unit, i, aDayNames[i], intInstructorId));
            }
        }
    }

    //---------------------------------------------------------------
    // Author : Adam Shaver
    // Class : InitalInstructorAvailVM
    // Abstract : ViewModel for setting the initial availability of an
    //            instructor.  This is then represented as a rolling week
    //            to week calendar.
    //---------------------------------------------------------------
    public class InitialInstructorAvailVM
    {
        public int intDayCode { get; set; }
        public string strDayName { get; set; }
        public List<CInstructorAvailablityVM> lstHelpers { get; set; }
       
        // Constructor
        public InitialInstructorAvailVM()
        {
        } 

        // Constructor with overloads
        public InitialInstructorAvailVM(CUnitOfWork unit, int dayCode, string dayName, int instructorId)
        {
            lstHelpers = new List<CInstructorAvailablityVM>();
            intDayCode = dayCode;
            strDayName = dayName;
            InitHelpersList(unit, instructorId);
        }

        //-------------------------------------------------------------------------
        //  Name: InitHelpersList
        //  Abstract: A list of all day times for availability
        //-------------------------------------------------------------------------
        private void InitHelpersList(CUnitOfWork unit, int instructorId)
        {
            int intLessonLength = unit.BusinessSettingsRep.GetAll().Select(x => x.intLessonLengthInMins).FirstOrDefault();
            if (unit.InstructorAvailRep.GetAll(x => x.CInstructorID == instructorId).Count() == 0)
            {
                var oplength = unit.OpLengthRep.GetAll(x => x.intDayCode == this.intDayCode).FirstOrDefault();
                int intLessonsPerDay = Utility.CTimeUtility.CalculateLessonsPerDay(oplength.intStartTime, oplength.intEndTime, intLessonLength);
                for (int i = 1; i < intLessonsPerDay + 1; i++)
                {
                    if (oplength.blnIsOperational)
                    {
                        var availableVm = new CInstructorAvailablityVM();
                        availableVm.intStartTime = oplength.intStartTime + (intLessonLength * (i - 1));
                        availableVm.strStartTime = Utility.CTimeUtility.ConvertToReadable(oplength.intStartTime + (intLessonLength * (i - 1)));
                        availableVm.strEndTime = Utility.CTimeUtility.ConvertToReadable(oplength.intStartTime + intLessonLength * i);
                        availableVm.blnAvailable = true;
                        lstHelpers.Add(availableVm);
                    }
                }
            }
            else
            {
                int intDayOfTheWeek = (int)DateTime.Now.DayOfWeek;
                DateTime dtmStartDate = DateTime.Now.AddDays(intDayOfTheWeek * -1);
                DateTime dtmEndDate = dtmStartDate.AddDays(7);
                var days = unit.DayRep.GetAll(x => x.dtmDate >= dtmStartDate.Date && x.dtmDate <= dtmEndDate.Date).OrderBy(x => x.dtmDate).ToList();

                int intDayId = (intDayCode > days.Count - 1) ? days[0].CDayID : days[intDayCode].CDayID;
                var availability = unit.InstructorAvailRep.GetAll(x => x.CDayID == intDayId && x.CInstructorID == instructorId).OrderBy(x => x.intStartTime).ToList();
                
                foreach (var available in availability)
                {
                    var availabilityVm = new CInstructorAvailablityVM().MapViewModel(available, intLessonLength);
                    lstHelpers.Add(availabilityVm);
                }               
            }
        }
    }

    //---------------------------------------------------------------
    // Author : Adam Shaver
    // Class : CInstructorAvailabilityVM
    // Abstract : ViewModel for instructor availability
    //---------------------------------------------------------------
    public class CInstructorAvailablityVM
    {
        public int CInstructorAvailabilityID { get; set; }
        public int CDayID { get; set; }
        public int intStartTime { get; set; }
        public string strStartTime { get; set; }
        public string strEndTime { get; set; }
        public bool blnAvailable { get; set; }

        //-------------------------------------------------------------------------
        //  Name: MapViewModel
        //  Abstract: Map a view model from an instructor availability model
        //-------------------------------------------------------------------------
        public CInstructorAvailablityVM MapViewModel(CInstructorAvailability availability, int lessonLength)
        {
            this.CInstructorAvailabilityID = availability.CInstructorAvailabilityID;
            this.CDayID = availability.CDayID;
            this.intStartTime = availability.intStartTime;
            this.strStartTime = Utility.CTimeUtility.ConvertToReadable(this.intStartTime);
            this.strEndTime = Utility.CTimeUtility.ConvertToReadable((availability.intStartTime + lessonLength));
            this.blnAvailable = availability.blnAvailable;
            return this;
        }
    }
}