﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Enum : LessonStatus
// Abstract : Enumeration for Lesson Status
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;

namespace MusicSEC.ViewModels
{
    public enum LessonStatus
    {
        Canceled,
        Complete,
        First_Lesson,
        Needs_Makeup,
        Scheduled,
    }
}