﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CInstructorVM
// Abstract : ViewModel for the Instructor view
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicSEC.Models;
using MusicSEC.DAL;
using System.ComponentModel.DataAnnotations;

namespace MusicSEC.ViewModels
{
    public class CInstructorVM
    {
        public int? CInstructorID { get; set; }
        public int? CUserImageID { get; set; }
        public int intUserId { get; set; }
        public bool blnIsPublic { get; set; }
        public string strFileType { get; set; }
        public byte[] bytImage { get; set; }

        [Required(ErrorMessage="A first name is required")]
        public string strFirstName { get; set; }

        [Required(ErrorMessage="A last name is required")]
        public string strLastName { get; set; }

        [Required(ErrorMessage="An email address is required")]
        public string strEmailAddress { get; set; }

        [Required(ErrorMessage = "A phone number is required")]
        public string strPhoneNumber { get; set; }

        [Required(ErrorMessage = "An address is required")]
        public string strAddress { get; set; }

        [Required(ErrorMessage = "An city is required")]
        public string strCity { get; set; }

        [Required(ErrorMessage = "A zip code is required")]
        public string strZipCode { get; set; }

        [Required(ErrorMessage = "A state is required")]
        public string strState { get; set; }
        public string strProfileDescription { get; set; }
        public string strActive { get; set; }
        public string strLessonTypeArray { get; set; }
        public CUserImage ProfileImage { get; set; }
        public List<CLessonType> lstLessonTypes { get; set; }
   
        // Constructor
        public CInstructorVM()
        { 
        }

        //Constructor with overloads.
        private CInstructorVM(bool isPublic, string fileType, byte[] image, string firstName, string LastName,
                              string email, string phone, string address, string city, string zip, string state, 
                              string descr, string active)
        {
            this.blnIsPublic = isPublic;
            this.strFileType = fileType;
            this.bytImage = image;
            this.strFirstName = firstName;
            this.strLastName = LastName;
            this.strEmailAddress = email;
            this.strPhoneNumber = phone;
            this.strAddress = address;
            this.strCity = city;
            this.strZipCode = zip;
            this.strState = state;
            this.strProfileDescription = descr;
            this.strActive = active;

        }
        //Property to return instructor full name
        public string FullName
        {
            get { return strFirstName + " " + strLastName; }
        }

        //-------------------------------------------------------------------------
        //  Name: MapViewModel
        //  Abstract: Map a view model for the Instructor View
        //-------------------------------------------------------------------------
        public CInstructorVM MapViewModel(CUnitOfWork unit, int instructorId, bool isPublic)
        {
            var inst = unit.InstructorRep.FindById(instructorId);
            var img = unit.ImageRep.FindById(inst.CUserImageID ?? 0);
            string strState = unit.StateRep.GetAll(x => x.CStateID == inst.CStateID).Select(x => x.strStateName).FirstOrDefault();
            string strActive = (inst.blnActive) ? "True" : "False";
            string strFileType = (img != null) ? img.strFileType : "";
            byte[] bytImage = (img != null) ? img.bytImage : new byte[0];
            CInstructorVM ViewModel = new CInstructorVM(isPublic, strFileType, bytImage, inst.strFirstName, inst.strLastName,
                                                        inst.strEmailAddress, inst.strPhoneNumber, inst.strAddress, inst.strCity,
                                                        inst.strZipCode, strState, inst.strProfileDescription, strActive);
            ViewModel.CInstructorID = instructorId;
            ViewModel.CUserImageID = unit.InstructorRep.FindById(instructorId).CUserImageID;
            ViewModel.ProfileImage = unit.ImageRep.FindById(ViewModel.CUserImageID ?? 0);
            ViewModel.lstLessonTypes = unit.InstructorRep.FindById(instructorId).LessonTypes.ToList();

            return ViewModel;
        }

        //-------------------------------------------------------------------------
        //  Name: MapModel
        //  Abstract: Map instructor model from viewmodel.
        //-------------------------------------------------------------------------
        public CInstructor MapModel(CUnitOfWork unit, CInstructorVM viewModel, HttpPostedFileBase file)
        {
            var instructor = unit.InstructorRep.FindById(viewModel.CInstructorID ?? default(int));
            instructor = instructor ?? new CInstructor();
       //     instructor.CInstructorID = viewModel.CInstructorID ?? default(int);
            instructor.strFirstName = viewModel.strFirstName;
            instructor.strLastName = viewModel.strLastName;
            instructor.strEmailAddress = viewModel.strEmailAddress;
            instructor.strPhoneNumber = viewModel.strPhoneNumber;
            instructor.strAddress = viewModel.strAddress;
            instructor.strCity = viewModel.strCity;
            instructor.CStateID = unit.StateRep.GetAll(x => x.strStateName.Trim().ToUpper() == viewModel.strState.Trim().ToUpper()).
                                                Select(x => x.CStateID).FirstOrDefault();
            instructor.strZipCode = viewModel.strZipCode;
            instructor.strProfileDescription = viewModel.strProfileDescription;
            instructor.blnActive = (viewModel.strActive == null || viewModel.strActive.Trim().ToUpper() == "TRUE") ? true : false;
            if (file != null && file.ContentLength != 0)
            {
                instructor.ProfileImage = new CUserImage().ProcessBytes(file);
            }
            else
            {
                instructor.CUserImageID = viewModel.CUserImageID;
            }

            var lessonTypes = new List<CLessonType>(ParseLessonTypes(unit, viewModel.strLessonTypeArray));
            if (unit.LessonTypeRep.GetAll().SelectMany(x => x.Instructors.Where(y => y.CInstructorID == instructor.CInstructorID)).Count() == 0)
            {
                instructor.LessonTypes = lessonTypes;
            }
            else
            {
                UpdateLessonTypes(unit, instructor.CInstructorID, lessonTypes);
            }
         
            return instructor;
        }

        //-------------------------------------------------------------------------
        //  Name: ParseLessonTypes
        //  Abstract: break up comma delimited list and return generic list of values
        //-------------------------------------------------------------------------
        private List<CLessonType> ParseLessonTypes(CUnitOfWork unit, string lessonString)
        {
            List<CLessonType> lstLessonTypes = new List<CLessonType>();
            if (lessonString != null && lessonString != "")
            {
                string[] astrLessonStrings = lessonString.Split(',');

                for (int i = 0; i < astrLessonStrings.Length; i++)
                {
                    int intLessonId = Convert.ToInt32(astrLessonStrings[i]);
                    lstLessonTypes.Add(unit.LessonTypeRep.FindById(intLessonId));
                }       
            }
            return lstLessonTypes;
        }

        //-------------------------------------------------------------------------
        //  Name: UpdateLessonTypes
        //  Abstract: Update the database with LessonTypes for instructor.
        //-------------------------------------------------------------------------
        private void UpdateLessonTypes(CUnitOfWork unit, int instructorId, List<CLessonType> lstLessonTypes)
        {
            if (unit.InstructorRep.GetAll().Any(x => x.CInstructorID == instructorId))
            {
                var instructor = unit.InstructorRep.FindById(instructorId);
                foreach (var lessonType in lstLessonTypes)
                {
                    if (!instructor.LessonTypes.Any(x => x.CLessonTypeID == lessonType.CLessonTypeID))
                    {
                        instructor.LessonTypes.Add(lessonType);
                    }
                }

                List<CLessonType> lstRemovedLessonTypes = new List<CLessonType>();
                foreach (var lessonType in instructor.LessonTypes)
                {
                    if(!lstLessonTypes.Any(x => x.CLessonTypeID == lessonType.CLessonTypeID))
                    {
                        lstRemovedLessonTypes.Add(lessonType);
                    }
                }

                foreach (var lessonType in lstRemovedLessonTypes)
                {
                    instructor.LessonTypes.Remove(lessonType);
                }

            }
       
        }
    }
}