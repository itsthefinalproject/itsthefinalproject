namespace MusicSEC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class instructoravail : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CInstructorAvailability",
                c => new
                    {
                        CInstructorAvailabilityID = c.Int(nullable: false, identity: true),
                        CInstructorID = c.Int(nullable: false),
                        CDayID = c.Int(nullable: false),
                        intStartTime = c.Int(nullable: false),
                        blnAvailable = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CInstructorAvailabilityID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CInstructorAvailability");
        }
    }
}
