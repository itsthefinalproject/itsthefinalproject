namespace MusicSEC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateprefs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CUserLayoutPreferences", "strLayout", c => c.String(nullable: false, maxLength: 150));
            DropColumn("dbo.CUserLayoutPreferences", "strLayoutPath");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CUserLayoutPreferences", "strLayoutPath", c => c.String(nullable: false, maxLength: 150));
            DropColumn("dbo.CUserLayoutPreferences", "strLayout");
        }
    }
}
