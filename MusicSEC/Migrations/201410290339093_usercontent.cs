namespace MusicSEC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class usercontent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CUserContent", "strBackgroundColor", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CUserContent", "strBackgroundColor");
        }
    }
}
