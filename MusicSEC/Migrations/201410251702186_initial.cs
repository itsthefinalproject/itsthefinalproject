namespace MusicSEC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CBusinessSettings",
                c => new
                    {
                        CBusinessSettingsID = c.Int(nullable: false, identity: true),
                        strBusinessName = c.String(maxLength: 75),
                        strTermsOfAgreement = c.String(),
                        intLessonLengthInMins = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CBusinessSettingsID);
            
            CreateTable(
                "dbo.COperationsLength",
                c => new
                    {
                        COperationsLengthID = c.Int(nullable: false, identity: true),
                        intStartTime = c.Int(nullable: false),
                        intEndTime = c.Int(nullable: false),
                        intDayCode = c.Int(nullable: false),
                        CBusinessSettingsID = c.Int(nullable: false),
                        blnIsOperational = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.COperationsLengthID)
                .ForeignKey("dbo.CBusinessSettings", t => t.CBusinessSettingsID, cascadeDelete: true)
                .Index(t => t.CBusinessSettingsID);
            
            CreateTable(
                "dbo.CState",
                c => new
                    {
                        CStateID = c.Int(nullable: false, identity: true),
                        strStateName = c.String(maxLength: 75),
                    })
                .PrimaryKey(t => t.CStateID);
            
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.CUserLayoutPreferences",
                c => new
                    {
                        CUserLayoutPreferencesID = c.Int(nullable: false, identity: true),
                        strLayoutPath = c.String(nullable: false, maxLength: 150),
                        strBackgroundColor = c.String(maxLength: 75),
                        CUserImageID = c.Int(),
                    })
                .PrimaryKey(t => t.CUserLayoutPreferencesID)
                .ForeignKey("dbo.CUserImage", t => t.CUserImageID)
                .Index(t => t.CUserImageID);
            
            CreateTable(
                "dbo.CUserImage",
                c => new
                    {
                        CUserImageID = c.Int(nullable: false, identity: true),
                        strCaption = c.String(maxLength: 500),
                        strFileType = c.String(),
                        blnCaptionTop = c.Boolean(nullable: false),
                        bytImage = c.Binary(nullable: false),
                    })
                .PrimaryKey(t => t.CUserImageID);
            
            CreateTable(
                "dbo.CUserContent",
                c => new
                    {
                        CUserContentID = c.Int(nullable: false, identity: true),
                        strContentContainerID = c.String(nullable: false, maxLength: 150),
                        strContentHTML = c.String(),
                        CUserLayoutPreferencesID = c.Int(nullable: false),
                        CUserImageID = c.Int(),
                        blnIsBackgroundImage = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CUserContentID)
                .ForeignKey("dbo.CUserImage", t => t.CUserImageID)
                .ForeignKey("dbo.CUserLayoutPreferences", t => t.CUserLayoutPreferencesID, cascadeDelete: true)
                .Index(t => t.CUserImageID)
                .Index(t => t.CUserLayoutPreferencesID);
            
            CreateTable(
                "dbo.CInstructor",
                c => new
                    {
                        CInstructorID = c.Int(nullable: false, identity: true),
                        intUserID = c.Int(nullable: false),
                        strFirstName = c.String(nullable: false, maxLength: 75),
                        strLastName = c.String(nullable: false, maxLength: 75),
                        strEmailAddress = c.String(nullable: false, maxLength: 150),
                        strPhoneNumber = c.String(nullable: false, maxLength: 75),
                        strAddress = c.String(nullable: false, maxLength: 150),
                        strCity = c.String(nullable: false, maxLength: 75),
                        CStateID = c.Int(nullable: false),
                        strZipCode = c.String(nullable: false, maxLength: 75),
                        strProfileDescription = c.String(),
                        CUserImageID = c.Int(),
                        blnActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CInstructorID)
                .ForeignKey("dbo.CState", t => t.CStateID)
                .ForeignKey("dbo.CUserImage", t => t.CUserImageID)
                .Index(t => t.CStateID)
                .Index(t => t.CUserImageID);
            
            CreateTable(
                "dbo.CStudent",
                c => new
                    {
                        CStudentID = c.Int(nullable: false, identity: true),
                        CInstructorID = c.Int(nullable: false),
                        intUserID = c.Int(nullable: false),
                        strFirstName = c.String(nullable: false, maxLength: 75),
                        strLastName = c.String(nullable: false, maxLength: 75),
                        strParentsFirstName = c.String(maxLength: 75),
                        strEmailAddress = c.String(nullable: false, maxLength: 150),
                        strPhoneNumber = c.String(nullable: false, maxLength: 75),
                        strAddress = c.String(nullable: false, maxLength: 150),
                        strCity = c.String(nullable: false, maxLength: 75),
                        CStateID = c.Int(nullable: false),
                        strZipCode = c.String(nullable: false, maxLength: 75),
                        blnActive = c.Boolean(nullable: false),
                        blnFirstTime = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CStudentID)
                .ForeignKey("dbo.CState", t => t.CStateID)
                .ForeignKey("dbo.CInstructor", t => t.CInstructorID)
                .Index(t => t.CStateID)
                .Index(t => t.CInstructorID);
            
            CreateTable(
                "dbo.CLessonType",
                c => new
                    {
                        CLessonTypeID = c.Int(nullable: false, identity: true),
                        strLessonType = c.String(nullable: false, maxLength: 75),
                    })
                .PrimaryKey(t => t.CLessonTypeID);
            
            CreateTable(
                "dbo.CDay",
                c => new
                    {
                        CDayID = c.Int(nullable: false, identity: true),
                        dtmDate = c.DateTime(nullable: false),
                        intOperationsStartTime = c.Int(nullable: false),
                        intOperationsEndTime = c.Int(nullable: false),
                        blnOperational = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CDayID);
            
            CreateTable(
                "dbo.CStudio",
                c => new
                    {
                        CStudioID = c.Int(nullable: false, identity: true),
                        CStudioNameID = c.Int(nullable: false),
                        CDayID = c.Int(nullable: false),
                        intLessonSlotsPerDay = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CStudioID)
                .ForeignKey("dbo.CStudioName", t => t.CStudioNameID, cascadeDelete: true)
                .ForeignKey("dbo.CDay", t => t.CDayID, cascadeDelete: true)
                .Index(t => t.CStudioNameID)
                .Index(t => t.CDayID);
            
            CreateTable(
                "dbo.CStudioName",
                c => new
                    {
                        CStudioNameID = c.Int(nullable: false, identity: true),
                        strStudioName = c.String(maxLength: 75),
                    })
                .PrimaryKey(t => t.CStudioNameID);
            
            CreateTable(
                "dbo.CLesson",
                c => new
                    {
                        CLessonID = c.Int(nullable: false, identity: true),
                        CStudentID = c.Int(nullable: false),
                        CInstructorID = c.Int(nullable: false),
                        intStartTime = c.Int(nullable: false),
                        intEndTime = c.Int(nullable: false),
                        CStudioID = c.Int(nullable: false),
                        intStatus = c.Int(nullable: false),
                        strInternalComments = c.String(),
                    })
                .PrimaryKey(t => t.CLessonID)
                .ForeignKey("dbo.CStudent", t => t.CStudentID, cascadeDelete: true)
                .ForeignKey("dbo.CInstructor", t => t.CInstructorID, cascadeDelete: true)
                .ForeignKey("dbo.CStudio", t => t.CStudioID, cascadeDelete: true)
                .Index(t => t.CStudentID)
                .Index(t => t.CInstructorID)
                .Index(t => t.CStudioID);
            
            CreateTable(
                "dbo.InstructorLessonTypes",
                c => new
                    {
                        CLessonTypeID = c.Int(nullable: false),
                        CInstructorID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CLessonTypeID, t.CInstructorID })
                .ForeignKey("dbo.CLessonType", t => t.CLessonTypeID, cascadeDelete: true)
                .ForeignKey("dbo.CInstructor", t => t.CInstructorID, cascadeDelete: true)
                .Index(t => t.CLessonTypeID)
                .Index(t => t.CInstructorID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.InstructorLessonTypes", new[] { "CInstructorID" });
            DropIndex("dbo.InstructorLessonTypes", new[] { "CLessonTypeID" });
            DropIndex("dbo.CLesson", new[] { "CStudioID" });
            DropIndex("dbo.CLesson", new[] { "CInstructorID" });
            DropIndex("dbo.CLesson", new[] { "CStudentID" });
            DropIndex("dbo.CStudio", new[] { "CDayID" });
            DropIndex("dbo.CStudio", new[] { "CStudioNameID" });
            DropIndex("dbo.CStudent", new[] { "CInstructorID" });
            DropIndex("dbo.CStudent", new[] { "CStateID" });
            DropIndex("dbo.CInstructor", new[] { "CUserImageID" });
            DropIndex("dbo.CInstructor", new[] { "CStateID" });
            DropIndex("dbo.CUserContent", new[] { "CUserLayoutPreferencesID" });
            DropIndex("dbo.CUserContent", new[] { "CUserImageID" });
            DropIndex("dbo.CUserLayoutPreferences", new[] { "CUserImageID" });
            DropIndex("dbo.COperationsLength", new[] { "CBusinessSettingsID" });
            DropForeignKey("dbo.InstructorLessonTypes", "CInstructorID", "dbo.CInstructor");
            DropForeignKey("dbo.InstructorLessonTypes", "CLessonTypeID", "dbo.CLessonType");
            DropForeignKey("dbo.CLesson", "CStudioID", "dbo.CStudio");
            DropForeignKey("dbo.CLesson", "CInstructorID", "dbo.CInstructor");
            DropForeignKey("dbo.CLesson", "CStudentID", "dbo.CStudent");
            DropForeignKey("dbo.CStudio", "CDayID", "dbo.CDay");
            DropForeignKey("dbo.CStudio", "CStudioNameID", "dbo.CStudioName");
            DropForeignKey("dbo.CStudent", "CInstructorID", "dbo.CInstructor");
            DropForeignKey("dbo.CStudent", "CStateID", "dbo.CState");
            DropForeignKey("dbo.CInstructor", "CUserImageID", "dbo.CUserImage");
            DropForeignKey("dbo.CInstructor", "CStateID", "dbo.CState");
            DropForeignKey("dbo.CUserContent", "CUserLayoutPreferencesID", "dbo.CUserLayoutPreferences");
            DropForeignKey("dbo.CUserContent", "CUserImageID", "dbo.CUserImage");
            DropForeignKey("dbo.CUserLayoutPreferences", "CUserImageID", "dbo.CUserImage");
            DropForeignKey("dbo.COperationsLength", "CBusinessSettingsID", "dbo.CBusinessSettings");
            DropTable("dbo.InstructorLessonTypes");
            DropTable("dbo.CLesson");
            DropTable("dbo.CStudioName");
            DropTable("dbo.CStudio");
            DropTable("dbo.CDay");
            DropTable("dbo.CLessonType");
            DropTable("dbo.CStudent");
            DropTable("dbo.CInstructor");
            DropTable("dbo.CUserContent");
            DropTable("dbo.CUserImage");
            DropTable("dbo.CUserLayoutPreferences");
            DropTable("dbo.UserProfile");
            DropTable("dbo.CState");
            DropTable("dbo.COperationsLength");
            DropTable("dbo.CBusinessSettings");
        }
    }
}
