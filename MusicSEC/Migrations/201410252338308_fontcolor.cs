namespace MusicSEC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fontcolor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CUserLayoutPreferences", "strFontColor", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CUserLayoutPreferences", "strFontColor");
        }
    }
}
