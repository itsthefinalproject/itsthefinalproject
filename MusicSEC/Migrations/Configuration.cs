namespace MusicSEC.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using MusicSEC.Utility;

    internal sealed class Configuration : DbMigrationsConfiguration<MusicSEC.DAL.CContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MusicSEC.DAL.CContext context)
        {           
            CSeedUtility.SeedStates();
            CSeedUtility.SeedRoles();
            CSeedUtility.SeedBusinessSettings();
            CSeedUtility.SeedAdministrator();
        }
    }
}
