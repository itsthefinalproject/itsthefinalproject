﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CInstructorController
// Abstract : Controller for Instructors
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.DAL;
using MusicSEC.Models;
using System.Web.Security;
using MusicSEC.ViewModels;
using MusicSEC.Utility;

namespace MusicSEC.Controllers
{
    public class CInstructorController : Controller
    {
        private CUnitOfWork unit = new CUnitOfWork();

        //--------------------------------------------------------------
        // Name : InstructorProfile
        // Abstract : return the instructor profile view
        //--------------------------------------------------------------
        public ActionResult InstructorProfile(int instructorId = 0, bool blnIsPublic = true)
        {
            var instructor = unit.InstructorRep.FindById(instructorId);
  
            if (instructor == null)
            {
                ViewBag.States = new SelectList(unit.StateRep.GetAll(), "strStateName", "strStateName");
                ViewBag.intLessonTypeID = new SelectList(unit.LessonTypeRep.GetAll().OrderBy(x => x.strLessonType), "CLessonTypeID", "strLessonType");
                CInstructorVM instructorVM = new CInstructorVM();
                return PartialView("CreateInstructor", instructorVM);
            }

            return PartialView(new CInstructorVM().MapViewModel(unit, instructor.CInstructorID, 
                       (unit.IsAdministrator || unit.CurrentUserID == instructor.intUserID) ? false : true));
        }

        //--------------------------------------------------------------
        // Name : CreateInstructor
        // Abstract : return instructor view
        //--------------------------------------------------------------
        [HttpGet]
        public ActionResult CreateInstructor()
        {
            ViewBag.States = new SelectList(unit.StateRep.GetAll(), "strStateName", "strStateName");
            ViewBag.intLessonTypeID = new SelectList(unit.LessonTypeRep.GetAll().OrderBy(x => x.strLessonType), "CLessonTypeID", "strLessonType");
            CInstructorVM instructorVM = new CInstructorVM();
            return PartialView(instructorVM);
           // return View(instructorVM);
        }

        //--------------------------------------------------------------
        // Name : CreateInstructor POST
        // Abstract : create an instructor
        //--------------------------------------------------------------
        [Authorize(Roles = "Admin, Instructor")]
        [HttpPost]
        public ActionResult CreateInstructor(CInstructorVM cInstructorVM)
        {
            
            HttpPostedFileBase file = Request.Files[0];
            var instructor = cInstructorVM.MapModel(unit, cInstructorVM, file);
      //      var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToList();
            if (ModelState.IsValid)
            {
                instructor.intUserID = unit.CurrentUserID;
                unit.InstructorRep.Insert(instructor);
                bool worked = unit.Save();
            }
    //      return RedirectToAction("InstructorProfile", new { instructorId = instructor.CInstructorID });
            TempData["hdnDashMarker"] = "yes";
            return RedirectToAction("Dash", "Dashboard");
           
        }

        //--------------------------------------------------------------
        // Name : EditInstructor
        // Abstract : return edit view
        //--------------------------------------------------------------
        [HttpGet]
        public ActionResult EditInstructor(int id)
        {
            var instructorVM = new CInstructorVM().MapViewModel(unit, id, false);
            ViewBag.States = new SelectList(unit.StateRep.GetAll(), "strStateName", "strStateName");
            ViewBag.intLessonTypeID = new SelectList(unit.LessonTypeRep.GetAll().OrderBy(x => x.strLessonType), "CLessonTypeID", "strLessonType");
            return PartialView(instructorVM);
        }

        //--------------------------------------------------------------
        // Name : Edit Instructor Post
        // Abstract : save changes
        //--------------------------------------------------------------
        [HttpPost]
        public ActionResult EditInstructor(CInstructorVM cInstructorVM)
        {
            HttpPostedFileBase file = (Request.Files.Count > 0) ? Request.Files[0] : null;
            var instructor = cInstructorVM.MapModel(unit, cInstructorVM, file);
            unit.InstructorRep.Update(instructor);
            unit.Save();

            return RedirectToAction("InstructorProfile", new { instructorId = instructor.CInstructorID });
        }

        //--------------------------------------------------------------
        // Name : ManageInstructors
        // Abstract : Manage instructors for admin
        //--------------------------------------------------------------
        [HttpGet]
        public ActionResult ManageInstructors()
        {
            var lstInstructors = unit.InstructorRep.GetAll().
                                                    Select(x => new CInstructorVM().MapViewModel(unit, x.CInstructorID, false));

            return PartialView(lstInstructors);
        }

        //--------------------------------------------------------------
        // Name : InviteInstructor
        // Abstract : invite a new instructor to the academy
        //--------------------------------------------------------------
        public ActionResult InviteInstructor(string email)
        {
            if (email != "")
            {
                string strBusiness = unit.BusinessSettingsRep.GetAll().
                                                              Select(x => x.strBusinessName).
                                                              FirstOrDefault();

                // this works commented out to test just the email sending
                string[] astrUserInfo = CSeedUtility.SeedNewInstructor(strBusiness, email);
                bool blnSuccess = true;

                if (astrUserInfo[0] != null && astrUserInfo[0] != "" &&
                   astrUserInfo[1] != null && astrUserInfo[1] != "")
                {
                    blnSuccess = CEmailUtility.SendEmail(email, astrUserInfo[0], astrUserInfo[1], strBusiness);
                }
            }
            //TODO
            //Implement viewdata object to tell user if operation was completed successfully
            return RedirectToAction("ManageInstructors");
        }

        //--------------------------------------------------------------
        // Name : ProcessActiveInstructors
        // Abstract : set instructor to or from active status
        //--------------------------------------------------------------
        [HttpPost]
        public ActionResult ProcessActiveInstructors(string active, string inactive)
        {
            string[] astrActive = active.Split(',');
            string[] astrInActive = inactive.Split(',');
            UpdateActiveInstructors(astrActive, true);
            UpdateActiveInstructors(astrInActive, false);

            return RedirectToAction("ManageInstructors");
        }

        //--------------------------------------------------------------
        // Name : InstructorBrowser
        // Abstract : return all instructors in dialog browser
        //--------------------------------------------------------------
        public ActionResult InstructorBrowser()
        {
            var lstInstructors = unit.InstructorRep.GetAll().Select(x => new CInstructorVM().MapViewModel(unit, x.CInstructorID, true)).OrderBy(x => x.strLastName);
            return PartialView(lstInstructors);
        }

        //--------------------------------------------------------------
        // Name : InstructorAvailability
        // Abstract : return instructor availability view
        //--------------------------------------------------------------
        [HttpGet]
        public ActionResult InstructorAvailability(int instructorId = 0)
        {
            var availabilityVM = new InitialInstructorAvailVMList(unit, instructorId);
            return PartialView(availabilityVM);
        }

        //--------------------------------------------------------------
        // Name : InstructorAvailability
        // Abstract : save instructor availability
        //--------------------------------------------------------------
        [HttpPost]
        public ActionResult InstructorAvailability(string[] dayCode, string chkValues, string[] startTime )
        {
            string[] astrValues = chkValues.Split(',');

            int intInstructorId = SetAvailability(dayCode, astrValues, startTime);

            return RedirectToAction("InstructorAvailability", new { instructorId = intInstructorId });
        }

        //--------------------------------------------------------------
        // Name : UpdateActiveInstructors
        // Abstract : Update Instructors to Active status
        //--------------------------------------------------------------
        private void UpdateActiveInstructors(string[] instructorList, bool isActive)
        {
            bool blnActiveOpp = !isActive;

            for (int i = 0; i < instructorList.Length; i++)
            {
                int intInstructorId = (instructorList[i] != "") ? Convert.ToInt32(instructorList[i]) : - 1;
                if (unit.InstructorRep.GetAll().Any(x => x.CInstructorID == intInstructorId && x.blnActive == blnActiveOpp))
                {
                    var instructor = unit.InstructorRep.FindById(intInstructorId);
                    if (instructor != null)
                    {
                        instructor.blnActive = isActive;
                        unit.InstructorRep.Update(instructor);
                    }
                }
            }

            unit.Save();
        }

        //--------------------------------------------------------------
        // Name : SetAvailablity
        // Abstract : set the availability of instructor
        //--------------------------------------------------------------
        private int SetAvailability(string[] dayCode, string[] chkValues, string[] startTime)
        {
            var lstAvailable = new List<CInstructorAvailability>();
            int intUserId = unit.CurrentUserID;
            int intInstructorId = unit.InstructorRep.GetAll(x => x.intUserID == intUserId).
                                                     Select(x => x.CInstructorID).
                                                     FirstOrDefault();

            DateTime dtmDate = DateTime.Now.AddDays(-8);
            var days = unit.DayRep.GetAll(x => x.dtmDate >= dtmDate.Date).ToList();
            int intCnt = 0;
            // TEST 1
            var instructoravail = unit.InstructorAvailRep.GetAll(x => x.CInstructorID == intInstructorId).ToList();
            //END TEST
            foreach (var day in days)// unit.DayRep.GetAll(x => x.dtmDate >= dtmDate).ToList())
            {
                intCnt++;
                if(day.blnOperational)
                {
                    int intDayCode = (int)day.dtmDate.DayOfWeek;
                    var lstIndexes = DayCodeIndex(dayCode, intDayCode);

                    foreach (var index in lstIndexes)
                    {
                        bool blnExists = true;
                        int intStartTime = Convert.ToInt32(startTime[index]);
                        //Test 1
                        //var available = unit.InstructorAvailRep.GetAll(x => x.CDayID == day.CDayID && 
                        //                                                    x.CInstructorID == intInstructorId && 
                        //                                                    x.intStartTime == intStartTime).
                        //                                        FirstOrDefault();
                        //TEST 1
                        var available = instructoravail.Where(x => x.CDayID == day.CDayID && x.intStartTime == intStartTime).FirstOrDefault();
                        if (available == null)
                        {
                            available = new CInstructorAvailability();
                            blnExists = false;
                        }

                        if (blnExists)
                        {
                            bool blnOrigAvailable = available.blnAvailable;
                            available.blnAvailable = (chkValues[index] == "true") ? true : false;
                            //available.intStartTime = intStartTime;
                            //available.CDayID = day.CDayID;
                            //available.CInstructorID = intInstructorId;

                            if (blnOrigAvailable != available.blnAvailable)
                            {
                                available.intStartTime = intStartTime;
                                available.CDayID = day.CDayID;
                                available.CInstructorID = intInstructorId;
                                unit.InstructorAvailRep.Update(available);
                                //if (blnExists)
                                //{
                                //    unit.InstructorAvailRep.Update(available);
                                //}
                                //else
                                //{
                                //    unit.InstructorAvailRep.Insert(available);
                                //}
                            }
                        }
                        else
                        {
                            available.blnAvailable = (chkValues[index] == "true") ? true : false;
                            available.intStartTime = intStartTime;
                            available.CDayID = day.CDayID;
                            available.CInstructorID = intInstructorId;
                            unit.InstructorAvailRep.Insert(available);
                        }
                    }
                }
            }
            unit.Save();

            return intInstructorId;

           
        }

        //--------------------------------------------------------------
        // Name : DayCodeIndex
        // Abstract : return list of day codes
        //--------------------------------------------------------------
        private List<int> DayCodeIndex(string[] dayCodes, int dayCode)
        {
            var lstIndexes = new List<int>();
            for (int i = 0; i < dayCodes.Length; i++)
            {
                if (Convert.ToInt32(dayCodes[i]) == dayCode)
                {
                    lstIndexes.Add(i);
                }
            }
            return lstIndexes;
        }
    }
}
