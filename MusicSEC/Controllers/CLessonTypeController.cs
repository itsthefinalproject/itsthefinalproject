﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CLessonTypeController
// Abstract : Controller for LessonTypes
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.Models;
using MusicSEC.DAL;

namespace MusicSEC.Controllers
{
    public class CLessonTypeController : Controller
    {
        CUnitOfWork unit = new CUnitOfWork();
       //
        // GET: /CLessonType/
        //--------------------------------------------------------------
        // Name : Index
        // Abstract : return list of all lesson type view
        //--------------------------------------------------------------
        public ActionResult Index()
        {
            return PartialView(unit.LessonTypeRep.GetAll());
        }

        //
        // GET: /CLessonType/Details/5
        //--------------------------------------------------------------
        // Name : Details
        // Abstract : return the details view
        //--------------------------------------------------------------
        public ActionResult Details(int id = 0)
        {
            CLessonType clessontype = unit.LessonTypeRep.FindById(id);
            if (clessontype == null)
            {
                return HttpNotFound();
            }
            return PartialView(clessontype);
        }

        //
        // GET: /CLessonType/Create
        //--------------------------------------------------------------
        // Name : Create
        // Abstract : return the create view
        //--------------------------------------------------------------
        public ActionResult Create()
        {
            return PartialView();
        }

        //
        // POST: /CLessonType/Create
        //--------------------------------------------------------------
        // Name : Create POST
        // Abstract : create lesson type
        //--------------------------------------------------------------
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CLessonType clessontype)
        {
            if (ModelState.IsValid && !unit.LessonTypeRep.GetAll().Any(x => x.strLessonType.Trim().ToUpper() == clessontype.strLessonType.Trim().ToUpper()))
            {
                unit.LessonTypeRep.Insert(clessontype);
                unit.Save();
                return RedirectToAction("Index", clessontype);
            }

            return PartialView(clessontype);
        }

        //
        // GET: /CLessonType/Edit/5
        //--------------------------------------------------------------
        // Name : Edit
        // Abstract : return the edit view
        //--------------------------------------------------------------
        public ActionResult Edit(int id = 0)
        {
            CLessonType clessontype = unit.LessonTypeRep.FindById(id);
            if (clessontype == null)
            {
                return HttpNotFound();
            }
            return PartialView(clessontype);
        }

        //
        // POST: /CLessonType/Edit/5
        //--------------------------------------------------------------
        // Name : Edit POST
        // Abstract : save changes
        //--------------------------------------------------------------
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CLessonType clessontype)
        {
            if (ModelState.IsValid)
            {
                unit.LessonTypeRep.Update(clessontype);
                unit.Save();
                return RedirectToAction("Index");
            }
            return PartialView(clessontype);
        }

        //
        // GET: /CLessonType/Delete/5
        //--------------------------------------------------------------
        // Name : Delete
        // Abstract : return delete view
        //--------------------------------------------------------------
        public ActionResult Delete(int id = 0)
        {
            CLessonType clessontype = unit.LessonTypeRep.FindById(id);
            if (clessontype == null)
            {
                return HttpNotFound();
            }
            return PartialView(clessontype);
        }

        //
        // POST: /CLessonType/Delete/5
        //--------------------------------------------------------------
        // Name : Delete POST
        // Abstract : delete the lesson type
        //--------------------------------------------------------------
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CLessonType clessontype = unit.LessonTypeRep.FindById(id);
            unit.LessonTypeRep.Remove(clessontype);
            unit.Save();
            return RedirectToAction("Index");
        }
        //--------------------------------------------------------------
        // Name : Dispose
        // Abstract : Dispose of contoller
        //--------------------------------------------------------------
        protected override void Dispose(bool disposing)
        {
            unit.Dispose();
            base.Dispose(disposing);
        }
    }
}