﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CStudioName Controller
// Abstract : Controller for Studio Names
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.Models;
using MusicSEC.DAL;

namespace MusicSEC.Controllers
{
    public class CStudioNameController : Controller
    {
        private CUnitOfWork unit = new CUnitOfWork();

        //
        // GET: /CStudioName/
        //--------------------------------------------------------------
        // Name : Index
        // Abstract : return list of all studionames view
        //--------------------------------------------------------------
        public ActionResult Index()
        {
            return  PartialView(unit.StudioNameRep.GetAll().ToList());
        }

        //
        // GET: /CStudioName/Details/5
        //--------------------------------------------------------------
        // Name : Details
        // Abstract : individual studio name view
        //--------------------------------------------------------------
        public ActionResult Details(int id = 0)
        {
            CStudioName cstudioname = unit.StudioNameRep.FindById(id);
            if (cstudioname == null)
            {
                return HttpNotFound();
            }
            return PartialView(cstudioname);
        }

        //
        // GET: /CStudioName/Create
        //--------------------------------------------------------------
        // Name : create
        // Abstract : return create view
        //--------------------------------------------------------------
        public ActionResult Create()
        {
            return PartialView();
        }

        //
        // POST: /CStudioName/Create
        //--------------------------------------------------------------
        // Name : create
        // Abstract : create studio name
        //--------------------------------------------------------------
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CStudioName cstudioname)
        {
            if (ModelState.IsValid)
            {
                unit.StudioNameRep.Insert(cstudioname);
                unit.Save();
                return RedirectToAction("Index");
            }

            return PartialView(cstudioname);
        }

        //
        // GET: /CStudioName/Edit/5
        //--------------------------------------------------------------
        // Name : Edit
        // Abstract : return edit view
        //--------------------------------------------------------------
        public ActionResult Edit(int id = 0)
        {
            CStudioName cstudioname = unit.StudioNameRep.FindById(id);
            if (cstudioname == null)
            {
                return HttpNotFound();
            }
            return PartialView(cstudioname);
        }

        //
        // POST: /CStudioName/Edit/5
        //--------------------------------------------------------------
        // Name : Edit POST
        // Abstract : save changes
        //--------------------------------------------------------------
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CStudioName cstudioname)
        {
            if (ModelState.IsValid)
            {
                unit.StudioNameRep.Update(cstudioname);
                unit.Save();
                return RedirectToAction("Index");
            }
            return PartialView(cstudioname);
        }

        //
        // GET: /CStudioName/Delete/5
        //--------------------------------------------------------------
        // Name : Delete
        // Abstract : return delete view
        //--------------------------------------------------------------
        public ActionResult Delete(int id = 0)
        {
            CStudioName cstudioname = unit.StudioNameRep.FindById(id);
            if (cstudioname == null)
            {
                return HttpNotFound();
            }
            return PartialView(cstudioname);
        }

        //
        // POST: /CStudioName/Delete/5
        //--------------------------------------------------------------
        // Name : DeleteConfirmed
        // Abstract : delete studio name
        //--------------------------------------------------------------
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CStudioName cstudioname = unit.StudioNameRep.FindById(id);
            unit.StudioNameRep.Remove(cstudioname);
            unit.Save();
            return RedirectToAction("Index");
        }

        //--------------------------------------------------------------
        // Name : Dispose
        // Abstract : dispose of controller
        //--------------------------------------------------------------
        protected override void Dispose(bool disposing)
        {
            unit.Dispose();
            base.Dispose(disposing);
        }
    }
}