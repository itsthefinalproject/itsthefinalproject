﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CStudentController
// Abstract : Controller for Students
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.DAL;
using MusicSEC.Utility;
using MusicSEC.ViewModels;

namespace MusicSEC.Controllers
{
    public class CStudentController : Controller
    {
        CUnitOfWork unit = new CUnitOfWork();
        //
        // GET: /CStudent/
        //--------------------------------------------------------------
        // Name : StudentProfile
        // Abstract : return student profile by student id
        //--------------------------------------------------------------
        public ActionResult StudentProfile(int studentId = 0)
        {
            var student = unit.StudentRep.FindById(studentId);

            if (student == null)
            {
                return HttpNotFound();
            }
            ViewBag.StudentId = studentId;
            return PartialView(new CStudentVM().MapViewModel(unit, student));
        }

        //--------------------------------------------------------------
        // Name : CreateStudent
        // Abstract : return create student view
        //--------------------------------------------------------------
        [HttpGet]
        public ActionResult CreateStudent(int instructorId = 0)
        {
            ViewBag.States = new SelectList(unit.StateRep.GetAll(), "strStateName", "strStateName");
            ViewBag.TermsOfAgreement = unit.BusinessSettingsRep.GetAll().FirstOrDefault().strTermsOfAgreement;
            CStudentVM studentVM = new CStudentVM();

            if (instructorId != 0)
                studentVM.CInstructorID = instructorId;

            return View(studentVM);
        }

        //--------------------------------------------------------------
        // Name : CreateStudent POST
        // Abstract : create student
        //--------------------------------------------------------------
        [HttpPost]
        public ActionResult CreateStudent(CStudentVM cstudentVM, string acceptTerms, int hdnStudioId, int hdnStartTime)
        {

            if (acceptTerms == null)
            {
                ViewBag.States = new SelectList(unit.StateRep.GetAll(), "strStateName", "strStateName");
                ViewBag.TermsOfAgreement = unit.BusinessSettingsRep.GetAll().FirstOrDefault().strTermsOfAgreement;
                return View(cstudentVM);
            }
            var registerUtility = new CRegisterUtility();
            int intCurrentUserId = registerUtility.RegisterStudent(cstudentVM.strUserName, cstudentVM.strPassword);

            if (intCurrentUserId != -1)
            {
                var student = cstudentVM.MapModel(unit, cstudentVM);
                student.intUserID = intCurrentUserId;

                if (ModelState.IsValid)
                {
                    unit.StudentRep.Insert(student);
                    bool worked = unit.Save();
                }

                if (student.CStudentID != 0)
                {
                    var lesson = CLessonUtility.Instance.CreateLesson(unit, hdnStudioId, hdnStartTime, student.CStudentID, student.CInstructorID);
                    unit.LessonRep.Insert(lesson);
                    unit.Save();
                }

                return RedirectToAction("RegistrationConfirmation", cstudentVM);
            }
            else
            {
                return View();
            }
        }

        //--------------------------------------------------------------
        // Name : EditProfile
        // Abstract : return edit profile view
        //--------------------------------------------------------------
        [HttpGet]
        public ActionResult EditProfile(int studentId = 0)
        {
            var student = unit.StudentRep.FindById(studentId);

            if (student == null)
            {
                return HttpNotFound();
            }

            var studentVm = new CStudentVM().MapViewModel(unit, student);
            ViewBag.States = new SelectList(unit.StateRep.GetAll(), "strStateName", "strStateName", studentVm.strState);
            return PartialView(studentVm);
        }

        //--------------------------------------------------------------
        // Name : EditProfile POST
        // Abstract : save changes
        //--------------------------------------------------------------
        [HttpPost]
        public ActionResult EditProfile(CStudentVM studentVm)
        {
            var student = studentVm.MapModel(unit, studentVm);
            unit.StudentRep.Update(student);
            unit.Save();
            return RedirectToAction("StudentProfile", new { studentId = student.CStudentID });
        }

        //--------------------------------------------------------------
        // Name : RegistrationConfirmation
        // Abstract : confirm student registration
        //--------------------------------------------------------------
        public ActionResult RegistrationConfirmation(CStudentVM studentVM)
        {
            var instructorVM = new CInstructorVM().MapViewModel(unit, studentVM.CInstructorID, true);
            ViewBag.StudentName = studentVM.FullName;
            ViewBag.BusinessName = unit.BusinessName;
            ViewBag.InstructorName = instructorVM.FullName;

            return View();
        }

        //--------------------------------------------------------------
        // Name : ManageStudents
        // Abstract : return view to manage students
        //--------------------------------------------------------------
        [HttpGet]
        public ActionResult ManageStudents(int instructorId = 0)
        {
            var lstStudents = (instructorId == 0) ? unit.StudentRep.GetAll().Select(x => new CStudentVM().MapViewModel(unit, x)) :
                                                    unit.StudentRep.GetAll(x => x.CInstructorID == instructorId).Select(x => new CStudentVM().MapViewModel(unit, x));

            return PartialView(lstStudents);
        }

        //--------------------------------------------------------------
        // Name : ProcessActiveStudents
        // Abstract : process active students
        //--------------------------------------------------------------
        [HttpPost]
        public ActionResult ProcessActiveStudents(string active, string inactive)
        {
            string[] astrActive = active.Split(',');
            string[] astrInActive = inactive.Split(',');
            UpdateActiveStudents(astrActive, true);
            UpdateActiveStudents(astrInActive, false);

            return RedirectToAction("ManageStudents");
        }

        //--------------------------------------------------------------
        // Name : UpdateActiveStudents
        // Abstract : update active students
        //--------------------------------------------------------------
        private void UpdateActiveStudents(string[] studentList, bool isActive)
        {
            bool blnActiveOpp = !isActive;

            for (int i = 0; i < studentList.Length; i++)
            {
                int intStudentId = (studentList[i] != "") ? Convert.ToInt32(studentList[i]) : -1;
                if (unit.StudentRep.GetAll().Any(x => x.CStudentID == intStudentId && x.blnActive == blnActiveOpp))
                {
                    var student = unit.StudentRep.FindById(intStudentId);
                    if (student != null)
                    {
                        var utility = new CRegisterUtility();
                        student.blnActive = isActive;
                        if (isActive)
                        {
                            utility.EnrollStudent(unit, intStudentId);
                        }
                        unit.StudentRep.Update(student);
                    }
                }
            }

            unit.Save();
        }

     

    }
}
