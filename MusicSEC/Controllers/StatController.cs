﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : StatController
// Abstract : Controller for Academy Statistics
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.DAL;
using MusicSEC.Utility;
using MusicSEC.ViewModels;

namespace MusicSEC.Controllers
{
    public class StatController : Controller
    {
        CUnitOfWork unit = new CUnitOfWork();

        //--------------------------------------------------------------
        // Name : AcademyStat
        // Abstract : the academy stat view
        //--------------------------------------------------------------
        public ActionResult AcademyStat(AcademyStatVM vm = null, int statusId = 1, int orderId = 1, string lastTab = "fs-tab-1")
        {
            if (vm.dtmStartDate.Year < 1988)
            {
                vm.dtmStartDate = DateTime.Now.AddDays(-7).Date;
            }
            if (vm.dtmEndDate.Year < 1988)
            {
                vm.dtmEndDate = DateTime.Now.AddDays(7).Date;
            }

            var academyStat = new AcademyStatVM(vm.dtmStartDate, vm.dtmEndDate).MapViewModel(unit);
            var dctsStatus = CLessonUtility.Instance.LessonStatus();
            var dctLessonOrder = CLessonUtility.Instance.LessonOrder();
            ViewBag.ddlStatus = new SelectList(dctsStatus, "key", "value", statusId);
            ViewBag.ddlLessonOrder = new SelectList(dctLessonOrder, "key", "value", orderId);
            ViewBag.LastTab = lastTab;
            return PartialView(academyStat);
        }

        //--------------------------------------------------------------
        // Name : InstructorByLessonStatus
        // Abstract : return instructor name for lesson status filter criteria
        //--------------------------------------------------------------
        public string InstructorByLessonStatus(int statusId, int lessonOrder, DateTime start, DateTime end)
        {
            bool blnLessonOrder = (lessonOrder == 1) ? true :false;
            int intInstructorId = CLessonUtility.Instance.StatByStatus(unit, statusId, blnLessonOrder, start, end);
            string strFullName = unit.InstructorRep.GetAll(x => x.CInstructorID == intInstructorId).
                                                    Select(x => x.strFirstName + " " + x.strLastName).
                                                    FirstOrDefault() ?? "";
            return (strFullName == "") ? "no result" : strFullName;
        }


        
    }
}
