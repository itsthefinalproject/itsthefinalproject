﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CDayController
// Abstract : Controller for CDays
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.DAL;
using MusicSEC.Models;
using MusicSEC.Utility;
using MusicSEC.ViewModels;

namespace MusicSEC.Controllers
{
    public class CDayController : Controller
    {
        private CUnitOfWork unit = new CUnitOfWork();
        //
        // GET: /CDay/

        //--------------------------------------------------------------
        // Name : Details
        // Abstract : return day details view
        //--------------------------------------------------------------
        public ActionResult Details(int day, int month, int year)
        {
            var cday = unit.DayRep.GetAll(x => x.dtmDate.Day == day && 
                                               x.dtmDate.Month == month && 
                                               x.dtmDate.Year == year).
                                               FirstOrDefault();

            var cdayvm = new CDayVM().MapViewModel(cday, unit);
            if (unit.IsAdministrator)
            {
                ViewBag.IsAdmin = true;
                return PartialView("AdminDay", cdayvm);
            }
            else if (unit.IsInstructor)
            {
                ViewBag.IsInstructor = true;
                ViewBag.InstructorId = CUserUtility.Instance.GetCurrentInstructorId(unit);
                return PartialView("InstructorDay", cdayvm);
            }
            else if (unit.IsEnrolled)
            {
                ViewBag.IsEnrolled = true;
                ViewBag.StudentId = CUserUtility.Instance.GetCurrentStudentId(unit);
                return PartialView("StudentDay", cdayvm);
            }
            else
            {
                ViewBag.IsAnonymous = true;
            }

            
            return PartialView(cdayvm);
        }

        //--------------------------------------------------------------
        // Name : Finish
        // Abstract : Finish setting details
        //--------------------------------------------------------------
        public ActionResult Finish()
        {
            bool blnNeedsCompleted = true;

            if (unit.DayRep.GetAll().Count() > 0)
                blnNeedsCompleted = false;   
                
            ViewBag.NeedsCompleted = blnNeedsCompleted;

            return PartialView();
        }

        ///--------------------------------------------------------------
        // Name : GenerateDays
        // Abstract : call seed utility and generate calendar
        //--------------------------------------------------------------
        public ActionResult GenerateDays()
        {
            CSeedUtility.SeedCalendar(unit, DateTime.Now);
            return RedirectToAction("Finish");
        }

        //--------------------------------------------------------------
        // Name : UpdateDayTime
        // Abstract : update operational time for day
        //--------------------------------------------------------------
        [HttpPost]
        public ActionResult UpdateDayTime(CDayVM cdayvm)
        {
            if (cdayvm.CDayID != 0)
            {
                var oplength = cdayvm.OplengthVm;
                bool blnIsAm = (oplength.intStartAm == 0) ? true : false;
                bool blnIsPm = (oplength.intEndAm == 0) ? true : false;
                cdayvm.intStartTime = CTimeUtility.MinutesSinceMidnight(oplength.intStartHours, oplength.intStartMins, blnIsAm);
                cdayvm.intEndTime = CTimeUtility.MinutesSinceMidnight(oplength.intEndHours, oplength.intEndMins, blnIsPm);
                cdayvm.blnOperational = oplength.blnIsOperational;
                var day = cdayvm.MapModel(cdayvm, unit);

                unit.DayRep.Update(day);
                unit.Save();
            }

            return PartialView("Details", new { day = cdayvm.intDay, month = cdayvm.intMonth, year = cdayvm.intYear });
        }

    }
}
