﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : HomeController
// Abstract : Controller for Home page
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MusicSEC.Controllers
{
    public class HomeController : Controller
    {
        //--------------------------------------------------------------
        // Name : Index
        // Abstract : return main home page
        //--------------------------------------------------------------
        public ActionResult Index()
        {
            return View();
        }

        //--------------------------------------------------------------
        // Name : Instrucotrs
        // Abstract : view where potential students can look at instructors
        //            and sign up for lesson
        //--------------------------------------------------------------
        public ActionResult Instructors()
        {
            return View();
        }

        
    }
}
