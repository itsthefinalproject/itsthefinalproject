﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CBusinessSettingsController
// Abstract : Controller for Business Settings
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.ViewModels;
using MusicSEC.DAL;

namespace MusicSEC.Controllers
{
    public class CBusinessSettingsController : Controller
    {
        CUnitOfWork unit = new CUnitOfWork();
        //
        // GET: /CBusinessSettings/

        //--------------------------------------------------------------
        // Name : BusinessSettings
        // Abstract : return Business settings view
        //--------------------------------------------------------------
        public ActionResult BusinessSettings()
        {
            var businessSettings = new CBusinessSettingsVM().MapViewModel(unit);
            return PartialView(businessSettings);
        }

        //--------------------------------------------------------------
        // Name : Business settings POST
        // Abstract : save changes to the business settings
        //--------------------------------------------------------------
        [HttpPost]
        public ActionResult BusinessSettings(CBusinessSettingsVM businessVM, FormCollection collection)
        {
            var businessSettings = businessVM.MapModel(businessVM, unit);
            unit.BusinessSettingsRep.Update(businessSettings);
            unit.Save();
            return RedirectToAction("BusinessSettings");
        }


    }
}
