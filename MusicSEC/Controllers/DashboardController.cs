﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : DashboardController
// Abstract : Controller for Dashboard
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.ViewModels;
using MusicSEC.DAL;
using MusicSEC.Utility;

namespace MusicSEC.Controllers
{
    public class DashboardController : Controller
    {
        CUnitOfWork unit = new CUnitOfWork();
        //
        // GET: /Dashboard/
        //--------------------------------------------------------------
        // Name : Dash
        // Abstract : return correct dash for user role
        //--------------------------------------------------------------
        [Authorize(Roles = "Administrator, Instructor, EnrolledStudent")] 
        public ActionResult Dash()
        {
            int intUserId = unit.CurrentUserID;
            if (unit.IsAdministrator)
            {
                ViewBag.IsAdmin = true;
            }
            else if (unit.IsInstructor)
            {
                
                ViewBag.IsAdmin = false;
                ViewBag.IsInstructor = true;
                ViewBag.InstructorId = unit.InstructorRep.GetAll(x => x.intUserID == intUserId).
                                                          Select(x => x.CInstructorID).
                                                          FirstOrDefault();

               
            }
            else if (unit.IsEnrolled)
            {
                ViewBag.IsAdmin = false;
                ViewBag.IsInstructor = false;
                ViewBag.IsEnrolled = true;
                ViewBag.StudentId = unit.StudentRep.GetAll(x => x.intUserID == intUserId).
                                                    Select(x => x.CStudentID).
                                                    FirstOrDefault();
            }
            return View();
        }

        public bool HasCalendar()
        {
            return unit.DayRep.GetAll().Any();
        }

    }
}
