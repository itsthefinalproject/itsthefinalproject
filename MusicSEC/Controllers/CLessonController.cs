﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CLessonController
// Abstract : Controller for Lessons
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.DAL;
using MusicSEC.Models;
using MusicSEC.Utility;
using MusicSEC.ViewModels;
using System.Web.Script.Serialization;

namespace MusicSEC.Controllers
{
    public class CLessonController : Controller
    {
        private CUnitOfWork unit = new CUnitOfWork();

        //--------------------------------------------------------------
        // Name : Index
        // Abstract : return the index view
        //--------------------------------------------------------------
        public ActionResult Index()
        {
            return View();
        }

        //--------------------------------------------------------------
        // Name : ManageLessonInstructors
        // Abstract : manage lessons for instructors
        //--------------------------------------------------------------
        public ActionResult ManageLessonsInstructor(int instructorId, int statusId = 4, int daysOut = 7)
        {
            var dctLessonStatus = CLessonUtility.Instance.LessonStatus();
            var dctDaysOut = CLessonUtility.Instance.DaysOut();
            var lstLessonVms = CLessonUtility.Instance.LessonsByStatus(unit, instructorId, statusId, daysOut);

            ViewBag.LessonFilter = new SelectList(dctLessonStatus, "key", "value", statusId);
            ViewBag.DaysOut = new SelectList(dctDaysOut, "key", "value", daysOut);
            ViewBag.NewStudents = unit.LessonRep.GetAll(x => x.intStatus == 2 && x.CInstructorID == instructorId).Count();
            ViewBag.InstructorId = instructorId;

            if (lstLessonVms.Count > 0)
                lstLessonVms = lstLessonVms.OrderBy(x => x.dtmDate).ThenBy(x => x.intStartTime).ToList();

            return PartialView(lstLessonVms);
        }

        //--------------------------------------------------------------
        // Name : ReviewLessonsInstructor
        // Abstract : instructor lesson review view
        //--------------------------------------------------------------
        [HttpGet]
        public ActionResult ReviewLessonsInstructor(int instructorId)
        {
            var dctLessonStatus = CLessonUtility.Instance.LessonStatus();         
            var lstLessonVms = CLessonUtility.Instance.InstructorReview(unit, instructorId);
            ViewBag.LessonFilter = new SelectList(dctLessonStatus, "key", "value");
            ViewBag.InstructorId = instructorId;
            return PartialView(lstLessonVms);
        }

        //--------------------------------------------------------------
        // Name : ReviewLessonsInstructor POST
        // Abstract : save changes to reviewed lesson
        //--------------------------------------------------------------
        [HttpPost]
        public ActionResult ReviewLessonsInstructor(List<CLessonVM> lessons, string hdnLessonInfoList, int instructorId = 0)
        {
            var json = new JavaScriptSerializer();
            var dctObjs = json.DeserializeObject(hdnLessonInfoList);
            List<int> lstUpdatedLessons = new List<int>();
            if (lessons != null)
            {
                foreach (Dictionary<string, object> item in (dctObjs as object[]))
                {
                    int intLessonId = 0;
                    int intStudioId = 0;
                    int intStartTime = 0;
                    if (item.ContainsKey("LessonId") && item["LessonId"] != null && item["LessonId"].ToString() != "")
                    {
                        intLessonId = Convert.ToInt32(item["LessonId"]);
                        var originalLesson = unit.LessonRep.FindById(intLessonId);
                        var lesson = lessons.FirstOrDefault(x => x.CLessonID == intLessonId);
                        if (originalLesson != null && lesson != null)
                        {


                            if (item.ContainsKey("StudioId") && item["StudioId"] != null && item["StudioId"].ToString() != "")
                            {
                                intStudioId = Convert.ToInt32(item["StudioId"]);
                            }

                            if (item.ContainsKey("StartTime") && item["StartTime"] != null && item["StartTime"].ToString() != "")
                            {
                                intStartTime = Convert.ToInt32(item["StartTime"]);
                            }

                              if (lesson.intStatusId == 3)
                              {
                                  originalLesson.intStartTime = intStartTime;
                                  originalLesson.CStudioID = intStudioId;
                                  originalLesson.strInternalComments = lesson.strComments;
                                  originalLesson.intStatus = 4;
                              }
                              lstUpdatedLessons.Add(intLessonId);
                              unit.LessonRep.Update(originalLesson);
                        }
                    }
                }
                foreach (var lesson in lessons)
                {
                    var originalLesson = unit.LessonRep.FindById(lesson.CLessonID);
                    if (originalLesson != null)
                    {
                        if (originalLesson.intStatus != lesson.intStatusId || originalLesson.strInternalComments != lesson.strComments)
                        {
                            if (!lstUpdatedLessons.Exists(x => x == lesson.CLessonID))
                            {
                                originalLesson.intStatus = lesson.intStatusId;
                                originalLesson.strInternalComments = lesson.strComments;
                                unit.LessonRep.Update(originalLesson);
                            }
                        }
                    }
                }

                
                unit.Save();
            }
            return RedirectToAction("ReviewLessonsInstructor", new { instructorId = instructorId });
        }

        //--------------------------------------------------------------
        // Name : ManageLessonsStudents
        // Abstract : Lesson management view for students
        //--------------------------------------------------------------
        public ActionResult ManageLessonsStudent(int studentId)
        {
            ViewBag.StudentId = studentId;
            ViewBag.InstructorId = unit.StudentRep.GetAll(x => x.CStudentID == studentId).
                                                   Select(x => x.CInstructorID).
                                                   FirstOrDefault();

            var lstLessonVms = CLessonUtility.Instance.StudentLessons(unit, studentId, 14);
            lstLessonVms = lstLessonVms.OrderBy(x => x.dtmDate).ThenBy(x => x.intStartTime).ToList();
            return PartialView(lstLessonVms);
        }

        //--------------------------------------------------------------
        // Name : StudentReschedule
        // Abstract : view for students to cancel/reschedule a lesson
        //--------------------------------------------------------------
        public ActionResult StudentReschedule(int instructorId, int studentId, int lessonId)
        {
            ViewBag.InstructorId = instructorId;
            ViewBag.StudentId = studentId;
            ViewBag.LessonId = lessonId;
            return PartialView();
        }

        //--------------------------------------------------------------
        // Name : VerifyReschedule
        // Abstract : save lesson reschedule for student
        //--------------------------------------------------------------
        [HttpPost]
        public ActionResult VerifyReschedule(int hdnInstructorId, int hdnStudentId, int hdnLessonId, int hdnStudioId, int hdnStartTime)  
        {
            var lesson = unit.LessonRep.FindById(hdnLessonId);
            lesson.intStartTime = hdnStartTime;
            lesson.CStudioID = hdnStudioId;
            unit.LessonRep.Update(lesson);
            unit.Save();

            return RedirectToAction("ManageLessonsStudent", new { studentId = hdnStudentId });
        }

        //--------------------------------------------------------------
        // Name : ScheduleLesson
        // Abstract : student schedule lesson
        //--------------------------------------------------------------
        [HttpPost]
        public ActionResult ScheduleLesson(int hdnInstructorId, int hdnStudentId, int hdnStudioId, int hdnStartTime)
        {
            var lesson = CLessonUtility.Instance.CreateLesson(unit, hdnStudioId, hdnStartTime, hdnStudentId, hdnInstructorId);
            unit.LessonRep.Insert(lesson);
            unit.Save();
            return RedirectToAction("ManageLessonsStudent", new { studentId = hdnStudentId });
        }

        //--------------------------------------------------------------
        // Name : LessonFilter
        // Abstract : return the lesson filter view
        //--------------------------------------------------------------
        public ActionResult LessonFilter()
        {
            return PartialView();
        }

        //--------------------------------------------------------------
        // Name : MultiLessonFilter
        // Abstract : lesson filter view that supports multi lesson editing
        //--------------------------------------------------------------
        public ActionResult MultiLessonFilter(int lessonId)
        {
            return PartialView(lessonId);
        }

        //--------------------------------------------------------------
        // Name : StudentsFilterLesson
        // Abstract : return lessons for students
        //--------------------------------------------------------------
        public JsonResult StudentsFilterLessons(string date1, string date2 = "", int instructorId = 0)
        {
            var lstLessonVms = new List<CLessonVM>();

            DateTime dtmDate1 = DateTime.TryParse(date1, out dtmDate1) ? DateTime.Parse(date1) : DateTime.Now; 

            if (date2 != "")
            {
                DateTime dtmDate2 = DateTime.TryParse(date2, out dtmDate2) ? DateTime.Parse(date2) : DateTime.Now;
                lstLessonVms = CLessonUtility.Instance.AvailLessonsByDateRange(unit, dtmDate1, dtmDate2, instructorId);
            }
            else
            {
                lstLessonVms = CLessonUtility.Instance.AvailLessonsByDate(unit, dtmDate1, instructorId);
            }


            var result = lstLessonVms.Where(x => x.blnAvailable == true).
                                      OrderBy(x => x.dtmDate).
                                      ThenBy(x => x.intStartTime).
                                      Select(x => new { 
                                            StudioId = x.CStudioID,
                                            Date = x.LessonDate(unit).ToShortDateString(),
                                            Start = CTimeUtility.ConvertToReadable(x.intStartTime),
                                            InternalStart = x.intStartTime,
                                            End = CTimeUtility.ConvertToReadable(x.intEndTime),
                                            Day = x.LessonDate(unit).DayOfWeek.ToString()
            }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //--------------------------------------------------------------
        // Name : InstructorsFilterLessons
        // Abstract : return lessons for instructors
        //--------------------------------------------------------------
        public JsonResult InstructorsFilterLessons(string date1, string date2 = "", int instructorId = 0)
        {
            var lstLessonVms = new List<CLessonVM>();

            DateTime dtmDate1 = DateTime.TryParse(date1, out dtmDate1) ? DateTime.Parse(date1) : DateTime.Now;

            if (date2 != "")
            {
                DateTime dtmDate2 = DateTime.TryParse(date2, out dtmDate2) ? DateTime.Parse(date2) : DateTime.Now;
                lstLessonVms = CLessonUtility.Instance.AvailLessonsByDateRange(unit, dtmDate1, dtmDate2, instructorId);
            }
            else
            {
                lstLessonVms = CLessonUtility.Instance.AvailLessonsByDate(unit, dtmDate1, instructorId);
            }

            var result = lstLessonVms.Select(x => new
            {
                StudioId = x.CStudioID,
                StudentId = x.CStudentID,
                Date = x.LessonDate(unit).ToShortDateString(),
                Start = CTimeUtility.ConvertToReadable(x.intStartTime),
                InternalStart = x.intStartTime,
                End = CTimeUtility.ConvertToReadable(x.intEndTime),
                Day = x.LessonDate(unit).DayOfWeek.ToString()
            });

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //--------------------------------------------------------------
        // Name : StudentSchedule
        // Abstract : return student schedule view
        //--------------------------------------------------------------
        public ActionResult StudentSchedule(int studentId, int instructorId)
        {
            ViewBag.StudentId = studentId;
            ViewBag.InstructorId = instructorId;
            return PartialView();
        }

        //--------------------------------------------------------------
        // Name : EmailStudent
        // Abstract : return dialog view for email dialog
        //--------------------------------------------------------------
        public ActionResult EmailStudent(int studentId)
        {
            ViewBag.EmailAddress = unit.StudentRep.FindById(studentId).strEmailAddress;
            return PartialView("_EmailStudent");

        }

        //--------------------------------------------------------------
        // Name : SendEmail
        // Abstract : instructor send email to student
        //--------------------------------------------------------------
        public bool SendEmail(string subject, string body, string emailAddress)
        {
            return CEmailUtility.EmailStudent(subject, body, emailAddress);
        }
    }
}
