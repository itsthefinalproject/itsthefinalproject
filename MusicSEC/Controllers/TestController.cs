﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : TESTController
// Abstract : Test controller for tests
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.Models;
using MusicSEC.ViewModels;
using MusicSEC.Utility;
using MusicSEC.DAL;
using System.IO;
using System.Web.Configuration;


namespace MusicSEC.Controllers
{
    public class TestController : Controller
    {
        //
        // GET: /Test/
        public CUnitOfWork unit = new CUnitOfWork();

        public ActionResult Index()
        {
            
            //ViewBag.Images = unit.ImageRep.GetAll().ToList();
            string astring = CLayoutUtility.BackgroundImgSrc();
            CCalendarVM vm = new CCalendarVM(11, 2014, unit);
            return View(vm);
        }

        [HttpPost]
        public ActionResult Upload()
        {
            HttpPostedFileBase file = Request.Files[0];
            if (file != null && file.ContentLength > 0)
            {
                CUserLayoutPreferences prefs = new CUserLayoutPreferences();
                prefs.strLayout = "hello";

                unit.LayoutPrefsRep.Insert(prefs);
                unit.Save();

                CUserImage image = new CUserImage();

                var extension = Path.GetFileName(file.FileName);
                if (extension != null)
                    image.strFileType = extension.TrimStart('.');

                //var height = Int32.Parse(WebConfigurationManager.AppSettings["UploadImageHeight"]);
                //var width = Int32.Parse(WebConfigurationManager.AppSettings["UploadImageWidth"]);

                image.bytImage = file.ConvertImageToBytes();
                unit.ImageRep.Insert(image);
                unit.Save();
                
            }

            return RedirectToAction("Index");
        }
        public ActionResult OperationsTest(COperationsLengthVM len)
        {
            COperationsLength length = len.MapModel(len, unit);
            return View("Index");
        }

        public JsonResult GetNonOperationalDays(int month, int year)
        {
         

            var days = unit.DayRep.GetAll(x => x.dtmDate.Month == month && 
                                          x.dtmDate.Year == year && 
                                          x.blnOperational == false).
                                          Select(x => x.dtmDate.Day).ToList();


            return Json(days, JsonRequestBehavior.AllowGet);
        }
    }
}
