﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CalendarController
// Abstract : Controller for Calendar
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.DAL;

namespace MusicSEC.Controllers
{
    public class CalendarController : Controller
    {
        private CUnitOfWork unit = new CUnitOfWork();
        //
        // GET: /Calendar/

        //--------------------------------------------------------------
        // Name : AdminSchedule
        // Abstract : return administrator schedule calendar
        //--------------------------------------------------------------
        public ActionResult AdminSchedule()
        {
            return PartialView();
        }

        //--------------------------------------------------------------
        // Name : InstructorSchedule
        // Abstract : return the instructor schedule calendar
        //--------------------------------------------------------------
        public ActionResult InstructorSchedule()
        {
            return PartialView();
        }

        //--------------------------------------------------------------
        // Name : StudentSchedule
        // Abstract : return the student schedule calendar
        //--------------------------------------------------------------
        public ActionResult StudentSchedule()
        {
            return PartialView();
        }

        //--------------------------------------------------------------
        // Name : GetNonOperationalDays
        // Abstract : returns jsonresult of all non operational days for a month
        //--------------------------------------------------------------
        public JsonResult GetNonOperationalDays(int month, int year)
        {


            var days = unit.DayRep.GetAll(x => x.dtmDate.Month == month &&
                                          x.dtmDate.Year == year &&
                                          x.blnOperational == false).
                                          Select(x => x.dtmDate.Day).ToList();


            return Json(days, JsonRequestBehavior.AllowGet);
        }

    }
}
