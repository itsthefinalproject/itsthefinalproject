﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CUserImageController
// Abstract : Controller for User Images
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.DAL;
using MusicSEC.Models;

namespace MusicSEC.Controllers
{
    public class CUserImagesController : Controller
    {
        CUnitOfWork unit = new CUnitOfWork();
        //
        // GET: /CUserImages/
        //--------------------------------------------------------------
        // Name : DisplayPictureGrid
        // Abstract : return a grid of all pictures
        //--------------------------------------------------------------
        public ActionResult DisplayPictureGrid()
        {
            var images = unit.ImageRep.GetAll().ToList();
            return PartialView(images);
        }

        //--------------------------------------------------------------
        // Name : UploadImage
        // Abstract : users upload image view save
        //--------------------------------------------------------------
        public ActionResult UploadImage(bool enableCaptions = false, bool enableButtonPane = true)
        {
            var userImage = new CUserImage();
            ViewBag.EnableCaptions = enableCaptions;
            ViewBag.EnableButtonPane = enableButtonPane;
            return PartialView(userImage);
        }

        //--------------------------------------------------------------
        // Name : UploadImage
        // Abstract : users upload image view save
        //--------------------------------------------------------------
        [HttpPost]
        public ActionResult UploadImage(CUserImage image, FormCollection collection)
        {
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase file = Request.Files[0];
                image.strFileType = image.GetFileName(file);
                image.bytImage = image.ImageToBytes(file);
                unit.ImageRep.Insert(image);
                unit.Save();
            }

            return RedirectToAction("ManageImages");
        }

        //--------------------------------------------------------------
        // Name : ManageImages
        // Abstract : manage user image vies
        //--------------------------------------------------------------
        public ActionResult ManageImages()
        {
            var lstImages = unit.ImageRep.GetAll().ToList();
            return PartialView(lstImages);
        }

        //--------------------------------------------------------------
        // Name : DeleteImage
        // Abstract : remove image
        //--------------------------------------------------------------
        [HttpPost]
        public void DeleteImage(int imageId)
        {
            var image = unit.ImageRep.FindById(imageId);
            var instructor = unit.InstructorRep.GetAll(x => x.CUserImageID == imageId).FirstOrDefault();

            if (instructor != null)
            {
                instructor.CUserImageID = null;
                unit.InstructorRep.Update(instructor);
            }

            if (image != null)
            {
                unit.ImageRep.Remove(image);
                unit.Save();
            }
        }

        //--------------------------------------------------------------
        // Name : getImageInfo
        // Abstract : get image information
        //--------------------------------------------------------------
        public string getImageInfo(int imageId)
        {
            return unit.ImageRep.GetAll(x => x.CUserImageID == imageId).Select(x => x.strFileType).FirstOrDefault();
        }
    }

   
}
