﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : LayoutPreferencesController
// Abstract : Controller for Layout prefs
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.DAL;
using MusicSEC.Utility;
using MusicSEC.ViewModels;

namespace MusicSEC.Controllers
{
    public class CUserLayoutPreferencesController : Controller
    {
        CUnitOfWork unit = new CUnitOfWork();
        //
        // GET: /CUserLayoutPreferences/
        //--------------------------------------------------------------
        // Name : Index
        // Abstract : layout pref view
        //--------------------------------------------------------------
        public ActionResult Index()
        {
            return View();
        }

        //--------------------------------------------------------------
        // Name : LayoutPrefs 
        // Abstract : return layout preferences view
        //--------------------------------------------------------------
        [HttpGet]
        public ActionResult LayoutPrefs()
        {
            var layoutprefs = new CLayoutPreferencesVM().MapViewModel(unit);
            return PartialView(layoutprefs);
        }

        //--------------------------------------------------------------
        // Name : LayoutPrefs POST
        // Abstract : save changes to layout prefs
        //--------------------------------------------------------------
        [HttpPost]
        public ActionResult LayoutPrefs(CLayoutPreferencesVM cLayoutPreferencesVM)
        {
            var layoutprefs = cLayoutPreferencesVM.MapModel(unit, cLayoutPreferencesVM);
            if (unit.LayoutPrefsRep.GetAll().Count() > 0)
            {
                unit.LayoutPrefsRep.Update(layoutprefs);
            }
            else
            {
                unit.LayoutPrefsRep.Insert(layoutprefs);
            }

            unit.Save();
            return RedirectToAction("LayoutPrefs");
        }

        //--------------------------------------------------------------
        // Name : LayoutEditor
        // Abstract : return layout editor view
        //--------------------------------------------------------------
        public ActionResult LayoutEditor()
        {
            return View();
        }

        //--------------------------------------------------------------
        // Name : HtmlEditor
        // Abstract : the html editor view
        //--------------------------------------------------------------
        public ActionResult HtmlEditor()
        {
            return PartialView();
        }

        //--------------------------------------------------------------
        // Name : SaveUserContent
        // Abstract : save the user content for the layout editor
        //--------------------------------------------------------------
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveUserContent(string containerId, string html, string backgroundcolor, string isbackgroundimg, int? imageId = 0)
        {
            var userContent = unit.ContentRep.GetAll(x => x.strContentContainerID.Trim().ToUpper() == containerId.Trim().ToUpper()).FirstOrDefault();
            int intPreferenceId = unit.LayoutPrefsRep.GetAll().Select(x => x.CUserLayoutPreferencesID).FirstOrDefault();
            bool blnExists = true;

            if (userContent == null)
            {
                userContent = new Models.CUserContent();
                blnExists = false;
            }
            userContent.strContentContainerID = containerId.Trim();
            userContent.strContentHTML = html;
            userContent.strBackgroundColor = backgroundcolor;

            if (imageId != 0)
                userContent.CUserImageID = imageId;

            userContent.blnIsBackgroundImage = (isbackgroundimg == "false") ? false : true;
            userContent.CUserLayoutPreferencesID = intPreferenceId;

            if (blnExists)
                unit.ContentRep.Update(userContent);
            else
                unit.ContentRep.Insert(userContent);

            bool blnSuccess = unit.Save();

            return View("LayoutEditor");
        }

        //--------------------------------------------------------------
        // Name : BackgroundImage
        // Abstract : return main background image
        //--------------------------------------------------------------
        public string BackgroundImage()
        {
            return CLayoutUtility.BackgroundImgSrc();
        }

        //--------------------------------------------------------------
        // Name : BackgroundColor
        // Abstract : return main layout backgroundcolor
        //--------------------------------------------------------------
        public string BackgroundColor()
        {
            return CLayoutUtility.BackgroundColor;
        }

        //--------------------------------------------------------------
        // Name : FontColor
        // Abstract : return the font color
        //--------------------------------------------------------------
        public string FontColor()
        {
            return CLayoutUtility.FontColor;
        }

        //--------------------------------------------------------------
        // Name : GetUserContent
        // Abstract : get user content for layout
        //--------------------------------------------------------------
        public JsonResult GetUserContent()
        {       
            var result = unit.ContentRep.GetAll().Select(x => new 
            {
                strHtml = x.strContentHTML,
                strContainerId = x.strContentContainerID,
                blnHasPic = x.blnIsBackgroundImage,
                strBackColor = x.strBackgroundColor,
                strImgSrc = (x.blnIsBackgroundImage) ? CLayoutUtility.GenerateImgSrc(x.CUserImageID ?? 0) : ""  ?? ""  
            });

            var userContent = new 
            { 
                BackImgSrc = BackgroundImage(),
                BackColor = BackgroundColor(),
                FontColor = FontColor(),
                ContentContainers = result
            };

            return Json(userContent, JsonRequestBehavior.AllowGet);
        }

    }
}
