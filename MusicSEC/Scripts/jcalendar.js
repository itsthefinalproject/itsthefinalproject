﻿var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var daysInMonth = ['31', '28', '31', '30', '31', '30', '31', '31', '30', '31', '30', '31'];
var currentDate = new Date();

function Calendar(divId) {
    this.containerElement = document.getElementById(divId);
    this.containerElement.className = 'cal-table-container';
    this.month = currentDate.getMonth();
    this.year = currentDate.getFullYear();
    this.calendarTable = document.createElement('table');
    this.calendarTable.title = "Double click day for details";
    this.currentDay = 1;
    this.generateHTML();
};

Calendar.prototype.nextMonth = function (event) {
    this.Calendar.month++;
    if (this.Calendar.month > 11) {
        this.Calendar.year++;
        this.Calendar.month = 0;
    }

    this.Calendar.clearTable();
    this.Calendar.generateHTML();
};

Calendar.prototype.previousMonth = function (event) {
    this.Calendar.month--;
    if (this.Calendar.month < 0) {
        this.Calendar.year--;
        this.Calendar.month = 11;
    }

    this.Calendar.clearTable();
    this.Calendar.generateHTML();
};

Calendar.prototype.clearTable = function () {
    while(this.calendarTable.firstChild) this.calendarTable.removeChild(this.calendarTable.firstChild);
};

Calendar.prototype.generateHTML = function () {
    var calendar = this.calendarTable;
    var thead = document.createElement('thead');
    var monthHeader = this.createMonthHeader();
    var dayHeader = this.createDayHeader();
    thead.appendChild(monthHeader);
    thead.appendChild(dayHeader);
    calendar.appendChild(thead);
    calendar.className = 'cal-main-table';
    this.createBody(calendar);
    this.containerElement.appendChild(calendar);

};

Calendar.prototype.createMonthHeader = function () {
    var monthName = months[this.month];
    var headerRow = document.createElement('tr');
    var headerRight = document.createElement('th');
    var headerCenter = document.createElement('th');
    var headerLeft = document.createElement('th');
    var largeFont = document.createElement('h2');
    var btnPrevious = document.createElement('button');
    var btnNext = document.createElement('button');
    var txtPrevious = document.createTextNode('<');
    var txtNext = document.createTextNode('>');
    var txtTitle = document.createTextNode(monthName + " " + this.year + " ");

    btnPrevious.appendChild(txtPrevious);
    btnPrevious.addEventListener('click', { handleEvent: this.previousMonth, Calendar: this }, false);
    btnPrevious.className = 'cal-nav-btn-prev';
    btnPrevious.setAttribute('name', 'prev');
    headerRight.appendChild(btnPrevious);

    headerCenter.colSpan = '5';
    largeFont.appendChild(txtTitle);
    headerCenter.appendChild(largeFont);

    btnNext.appendChild(txtNext);
    btnNext.addEventListener('click', { handleEvent: this.nextMonth, Calendar: this }, false);
    btnNext.setAttribute('name', 'next');
    btnNext.className = 'cal-nav-btn-next';
    headerLeft.appendChild(btnNext);

    headerRow.appendChild(headerRight);
    headerRow.appendChild(headerCenter);
    headerRow.appendChild(headerLeft);

    return headerRow;
};

Calendar.prototype.createDayHeader = function () {
    var headerRow = document.createElement('tr');
    for (i = 0; i < 7; i++) {
        var daycell = document.createElement('th');
        var txtDayCell = document.createTextNode(days[i]);

        daycell.appendChild(txtDayCell);
        headerRow.appendChild(daycell);
        headerRow.className = 'cal-day-header';
    }
    return headerRow;
};

Calendar.prototype.createBody = function (calendar) {
    var tbody = document.createElement('tbody');
    var dayOne = new Date(this.year, this.month, 1);
    var startDay = dayOne.getDay();
    var monthLength = daysInMonth[this.month];
    var cnt = 1;

    tbody.className = 'cal-tbody';

    // account for leap year
    if (this.month == 1) {
        if(this.year % 4 == 0 && this.year % 100 != 0 || this.year % 400 == 0){
            monthLength = 29;
        }
    }

    //create days in calendar
    for(i = 0; i < 9; i++) {
        var row = document.createElement('tr');
        row.className = 'cal-body-row';
        for(n = 0; n < 7; n++) {
            var cell = document.createElement('td');
            if(cnt <= monthLength && (i > 0 || n >= startDay)){
                var p = document.createElement('p');
                var txtDay = document.createTextNode(cnt);

                cell.className = 'cal-body-cell';
                cell.setAttribute('data-day', cnt);
                p.className = 'cal-day-number';
                p.appendChild(txtDay);
                p.setAttribute('unselectable', 'on');
                
                cell.appendChild(p);
                cell.addEventListener('click', { handleEvent: this.setCurrentDay, Calendar : this, day : cnt }, false);
                cnt++;
            }
            else{
                cell.className = 'cal-empty-cell';
            }
            row.appendChild(cell);
        }
        tbody.appendChild(row);
        if(cnt > monthLength){
            break;
        }
        calendar.appendChild(tbody);
    }
};

Calendar.prototype.setCurrentDay = function(event) {
    this.Calendar.currentDay = this.day;
};

Calendar.prototype.setActive = function (dayNoArray) {
    $.each($('.cal-tbody tr td'), function (index, cell) {
        var cell = $(cell);
        cell.removeClass('cal-not-active');
        for (i = 0; i < dayNoArray.length; i++) {
            if (cell.data('day') == dayNoArray[i]) {
                cell.addClass('cal-not-active');
            }
        }
    });
};

Calendar.prototype.getCurrentDateInfo = function () {
    return { day : this.currentDay, month : this.month, year : this.year };
};