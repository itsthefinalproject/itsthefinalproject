﻿$(document).ready(function () {

    //var backgroundURL = '../CUserLayoutPreferences/BackgroundColor';
    //var fontURL = '../CUserLayoutPreferences/FontColor';
    //var imgUrl = '../CUserLayoutPreferences/BackgroundImage';
    var contentUrl = '../CUserLayoutPreferences/GetUserContent';



    //$.ajax({
    //    type: 'GET',
    //    url: backgroundURL,
    //    success: function (backgroundColor) {
    //        $('body').css('background-color', backgroundColor);
    //    }

    //});

    //$.ajax({
    //    type: 'GET',
    //    url: fontURL,
    //    success: function (fontColor) {
    //        $('body').css('color', fontColor);
    //    }
    //});

    //$.ajax({
    //    type: 'GET',
    //    url: imgUrl,
    //    success: function (imgSrc) {
    //        $('body').css('background-image', 'url("' + imgSrc + '")');
    //        $('body').css('background-repeat', 'no-repeat');
    //        $('body').css('background-', 'center');
    //    }
    //});

    $.ajax({
        type: 'GET',
        url: contentUrl,
        success: function (userContent) {
            $('body').css({
                'background-image': 'url("' + userContent.BackImgSrc + '")',
                'background-color': userContent.BackColor,
                'background-repeat': 'no-repeat',
                'background-position': 'center',
                'color': userContent.FontColor,
            });
            $('#msc-main-cnt-container').css('background-color', hexToRgb(userContent.BackColor));
            $('a').not('.msc-color-link').css('color', userContent.FontColor);
            applyContent(userContent.ContentContainers)
        }
    });


    function applyContent(htmlArray) {
        var contentContainers = $('[id^="msc-content-container-"]');
        var htmlArray = $(htmlArray);
        $.each(contentContainers, function (index, value) {
            var container = $(value);
            var index = getContainerIndex(container.attr('id'), htmlArray);
            if (index != null) {
                container.css({
                    'background-image': 'url("' + htmlArray[index].strImgSrc + '")',
                    'background-color': htmlArray[index].strBackColor,
                });
                container.html(htmlArray[index].strHtml);        
            }
        });
    };

    function getContainerIndex(containerId, containers) {
        for (i = 0; i < containers.length; i++){
            if (containers[i].strContainerId.toString().trim().toUpperCase() ==
               containerId.toString().trim().toUpperCase()) {
                return i;
            }
        }
        return null;
    };

    function hexToRgb(hex) {
        var result = parseInt(hex, 16);
        var r = (result >> 16) & 255;
        var g = (result >> 8) & 255;
        var b = result & 255;
        var a = .35;
        var rgba = 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')';
        return rgba;

    };
});