﻿var dirty = false;
enableBtnUpdate(dirty);

var sort1 = $('#sortable-one');
var sort2 = $('#sortable-two');

if (sort1.children('li').length >= sort2.children('li').length) {
    var newHeight = sort1.children('li').length * 250
    sort2.css('height', newHeight + 'px');
   
}
else {
    var newHeight = sort2.children('li').length * 250;
    sort1.css('height', newHeight + 'px');

 //   alert(newHeight + sort1.css('height'));
}



$(function () {
    $("#sortable-one, #sortable-two").sortable({
        connectWith: ".msc-connected",
        change: function () {
            dirty = true;
            enableBtnUpdate(dirty);
        }
    }).disableSelection();
});

$("#txtActiveSearch").keyup(function () {
    var active = $("input[name=rdActive]:checked").val();
    var sortableList = (active == 0) ? "#sortable-one li" : "#sortable-two li";

    $.each($(sortableList + " ul li"), function (index, li) {
        var li = $(li).children(".msc-name");
        if (li != null && li.text().trim().toUpperCase().match($("#txtActiveSearch").val().trim().toUpperCase())) {
            li.parents(sortableList).show();
        }
        else {
            li.parents(sortableList).hide();
        }            
    });
});

$("#btnUpdate").click(function () {
    var active = generateList("sortable-one");
    var inactive = generateList("sortable-two");
    updateLists(active, inactive);
    dirty = false;
    enableBtnUpdate(dirty);
});

function enableBtnUpdate(isDirty) {
    $("#btnUpdate").button({ disabled: !isDirty });
};

function generateList(selectorId) {
    var instructorList = "";
    $.each($("#" + selectorId + " li ul"), function (index, li) {
        var li = $(li);
        if (li.data("entity-id") != null) {
            instructorList += li.data("entity-id") + ",";
        }
    });
    return instructorList;
};

//function updateLists(activeList, inactiveList) {
//    $.post('@Url.Action("ProcessActiveInstructors", "CInstructor")', { active : activeList, inactive : inactiveList });
//}