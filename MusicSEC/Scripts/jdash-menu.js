﻿$('.msc-dash-nav a').on('click', function (event) {
    if ($(this).attr('id') != 'aLayoutEditor' && $(this).attr('href') != '/Dashboard/Dash') {
        event.preventDefault();
        hideCluster($(this).attr('href'));
    }
});

$('body').on('submit', 'form', function (event) {
    event.preventDefault();

    var file;
    if ($('#dash-cluster input[type=file]').parent().length > 0)
        file = $('#dash-cluster input[type=file]')[0].files[0];

    var formData = new FormData(this);

    if (file != null)
        formData.append(file.name, file);

    $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: formData,
        processData: false,
        contentType: false,
        success: function (result) {       
            $('#dash-cluster').html(result);
            if ($('#dash-cluster').children().find('#hdnDashMarker').val() == 'yes') {
                alert('ok');
                window.location.href = '/Dashboard/Dash';
            }

            $('#dash-cluster a').on('click', function (event) {
                event.preventDefault();
                hideCluster($(this).attr('href'));
            });
        },
        error: function () {
       //     alert('an error has occurred');
        }
    });



});

function hideCluster(url) {
    $('#dash-cluster').hide('slide', { direction : 'left', complete: function () { loadCluster(url) } }, 300);
    removeColorPicker();
};

function loadCluster(url) {
    $.ajax({
        type: 'GET',
        url: url,
        success: function (result) {
            $('#dash-cluster').html(result).show('slide', { direction : 'left' }, 300);
            $('#dash-cluster a').on('click', function (event) {
                event.preventDefault();
                hideCluster($(this).attr('href'));
            });
        },
        error: function () {
        //    alert("an error has occurred.");
        }
    });
};

function removeColorPicker() {
    if ($('.colorpicker'))
        $('.colorpicker').remove();
};

