﻿var intselectedLi = 0;
var unorderList = $("#msc-ul");
var dropdown = $("select[name=LessonTypes]");
var btnAdd = $("input[name=btnAddLessonType]");
var btnRemove = $("input[name=btnRemoveLessonType]");
var hdnLessonString = $("#hdnLessonArray");

btnAdd.click(addLesson);
btnRemove.click(removeLesson);
generateLessonTypeString();

function addLesson() {
    if (!lessonTypeExists(dropdown.val()) && dropdown.val() != "") {
        var strdropDownText = dropdown.find("option:selected").text();
        unorderList.append('<li data-lessonid="' + dropdown.val() + '">' + strdropDownText + '</li>');
        generateLessonTypeString();
    }
};

function removeLesson() {
    unorderList.children("li").filter('[data-lessonid="' + intselectedLi + '"]').remove();
    generateLessonTypeString();
};

function lessonTypeExists(ddvalue) {
    return (unorderList.children("li").filter('[data-lessonid="' + ddvalue + '"]').length > 0) ? true : false;
}

function generateLessonTypeString() {
    strLessonTypes = "";

    $.each(unorderList.children("li"), function (index, val) {
        strLessonTypes += $(val).attr("data-lessonid") + ",";
    });
    strLessonTypes = strLessonTypes.substring(0, strLessonTypes.length - 1);
    hdnLessonString.val(strLessonTypes);
};


unorderList.on("click", "li", function () {
    var li = $(this);
    intselectedLi = li.data("lessonid");
    unorderList.children("li").removeClass("msc-highlight");
    li.addClass("msc-highlight");

});

function previewImage() {
    var reader = new FileReader();
    reader.readAsDataURL(document.getElementById("fileUpload").files[0]);
    reader.onload = function (event) {
        document.getElementById("imgUploadPreview").src = event.target.result;
    };
};