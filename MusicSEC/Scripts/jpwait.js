﻿var pwait = function () {
    var mydiv = $('#pleaseWait');
    var messagelbl = $('#pleaseWait-lbl');

  //  alert(mydiv.attr('id') + ' ' + messagelbl.attr('id'));
    this.isEnabled = false;
    this.message = "Processing...";
    this.processing = false;
    this.setMessage = function (txt) {
        messagelbl.text(txt);
    };
    var p = this;
    mydiv.hide();
    $(document).on({
        ajaxStart: function () {
            p.processing = true;
            setTimeout(function () {
         //       alert('1' + p.isEnabled + ' ' + p.processing);
                if (p.isEnabled && p.processing) {
                    mydiv.show();
                }
            }, 1500);
            
        }, 
        ajaxStop: function () {
            //alert('done');
            p.processing = false;
            mydiv.hide();
        }
    });
};

pwait.prototype.setEnabled = function (enabled) {
 //   alert('enabled');
    this.isEnabled = enabled;
};

pwait.prototype.setMessage = function(message){
    this.message = message;
    this.setMessage(message);
};

