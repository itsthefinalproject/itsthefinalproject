﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CUnitOfWork
// Abstract : Unit of work for all repositories
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicSEC.Models;
using WebMatrix.WebData;
using System.Web.Security;
using System.Diagnostics;

namespace MusicSEC.DAL
{
    public class CUnitOfWork : IDisposable
    {
        private const string ADMINISTRATOR = "Administrator";
        private const string INSTRUCTOR = "Instructor";
        private const string ENROLLED_STUDENT = "EnrolledStudent";

        private CContext context = new CContext();
        private bool blnDisposed = false;

        private CGenRepository<UserProfile> usersRepository;

        private CGenRepository<CInstructor> instructorRepository;
        private CGenRepository<CInstructorAvailability> instructorAvailabilityRepository;
        private CGenRepository<CStudent> studentRepository;
        private CGenRepository<CStudioName> studioNameRepository;
        private CGenRepository<CStudio> studioRepository;
        private CGenRepository<CState> stateRepository;
        private CGenRepository<CUserImage> imagesRepository;
        private CGenRepository<CUserContent> contentRepository;
        private CGenRepository<CUserLayoutPreferences> layoutPrefsRepository;
        private CGenRepository<CDay> dayRepository;
        private CGenRepository<CLessonType> lessonTypeRepository;
        private CGenRepository<CLesson> lessonRepository;
        private CGenRepository<CBusinessSettings> businessSettingsRepository;
        private CGenRepository<COperationsLength> operationsLengthRepository;


        //--------------------------------------------------------------
        // Name : Properties
        // Abstract : Various getters and setters
        //--------------------------------------------------------------
        public int CurrentUserID
        {
            get { return WebSecurity.CurrentUserId; }
        }

        public string CurrentUserRole
        {
            get { return (Roles.GetRolesForUser().Length == 0) ? "" : Roles.GetRolesForUser()[0]; } 
        }

        public string[] CurrentUserRoles
        {
            get { return Roles.GetRolesForUser(); }
        }

        public bool IsAdministrator
        {
            get { return Roles.IsUserInRole(ADMINISTRATOR); }
        }

        public bool IsInstructor
        {
            get { return Roles.IsUserInRole(INSTRUCTOR); }
        }

        public bool IsEnrolled
        {
            get { return Roles.IsUserInRole(ENROLLED_STUDENT); }
        }

        public string BusinessName
        {
            get { return this.BusinessSettingsRep.GetAll().FirstOrDefault().strBusinessName; }
        }
        //---------------------------------------------------------------
        // Name : Repository Properties
        // Abstract : Implement Singleton pattern to ensure one context
        //            at a time.
        //---------------------------------------------------------------

        public CGenRepository<UserProfile> UsersRep
        {
            get { return this.usersRepository ?? new CGenRepository<UserProfile>(context); }
        }

        public CGenRepository<CInstructor> InstructorRep
        {
            get { return this.instructorRepository ?? new CGenRepository<CInstructor>(context); }            
        }

        public CGenRepository<CInstructorAvailability> InstructorAvailRep
        {
            get { return this.instructorAvailabilityRepository ?? new CGenRepository<CInstructorAvailability>(context); }
        }

        public CGenRepository<CStudent> StudentRep
        {
            get { return this.studentRepository ?? new CGenRepository<CStudent>(context); }
        }

        public CGenRepository<CStudioName> StudioNameRep
        {
            get { return this.studioNameRepository ?? new CGenRepository<CStudioName>(context); }
        }

        public CGenRepository<CStudio> StudioRep
        {
            get { return this.studioRepository ?? new CGenRepository<CStudio>(context); }
        }

        public CGenRepository<CState> StateRep
        {
            get { return this.stateRepository ?? new CGenRepository<CState>(context); }
        }

        public CGenRepository<CUserImage> ImageRep
        {
            get { return this.imagesRepository ?? new CGenRepository<CUserImage>(context); }
        }

        public CGenRepository<CUserContent> ContentRep
        {
            get { return this.contentRepository ?? new CGenRepository<CUserContent>(context); }
        }

        public CGenRepository<CUserLayoutPreferences> LayoutPrefsRep
        {
            get { return this.layoutPrefsRepository ?? new CGenRepository<CUserLayoutPreferences>(context); }
        }

        public CGenRepository<CDay> DayRep
        {
            get { return this.dayRepository ?? new CGenRepository<CDay>(context); }
        }

        public CGenRepository<CLessonType> LessonTypeRep
        {
            get { return this.lessonTypeRepository ?? new CGenRepository<CLessonType>(context); }
        }

        public CGenRepository<CLesson> LessonRep
        {
            get { return this.lessonRepository ?? new CGenRepository<CLesson>(context); }
        }

        public CGenRepository<CBusinessSettings> BusinessSettingsRep
        {
            get { return this.businessSettingsRepository ?? new CGenRepository<CBusinessSettings>(context); }
        }

        public CGenRepository<COperationsLength> OpLengthRep
        {
            get { return this.operationsLengthRepository ?? new CGenRepository<COperationsLength>(context); }
        }

        //---------------------------------------------------------------
        // Name: Save
        // Abstract: Save changes to the context 
        //---------------------------------------------------------------
        public bool Save()
        {
            bool blnSuccess = true;

            try
            {
                context.SaveChanges();
            }
            catch(System.Data.Entity.Validation.DbEntityValidationException e)
            {

                foreach (var validationErrors in e.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}",
                            validationErrors.Entry.Entity.GetType().FullName,
                            validationError.PropertyName,
                            validationError.ErrorMessage);
                    }
                }

                
                string needsTobeLogged = e.ToString();
                blnSuccess = false;
                throw; 
            }

            return blnSuccess;
        }

        //---------------------------------------------------------------
        // Name: Dispose
        // Abstract: Ensure context is properly disposed 
        //---------------------------------------------------------------
        protected virtual void Dispose(bool disposing)
        {
            if (!this.blnDisposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.blnDisposed = true;
        }

        //---------------------------------------------------------------
        // Name: Dispose
        // Abstract: Ensure context is properly disposed 
        //---------------------------------------------------------------
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}