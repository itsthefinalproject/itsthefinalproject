﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CContext
// Abstract : DB Context for MusicSEC 
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MusicSEC.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MusicSEC.DAL
{
    public class CContext : DbContext
    {
        public DbSet<CBusinessSettings> BusinessSettings { get; set; }
        public DbSet<COperationsLength> OperationsLength { get; set; }
        public DbSet<CState> States { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<CUserLayoutPreferences> UserLayoutPrefs { get; set; }
        public DbSet<CUserImage> UserImages { get; set; }
        public DbSet<CUserContent> UserContent { get; set; }
        public DbSet<CInstructor> Instructors { get; set; }
        public DbSet<CInstructorAvailability> InstructorAvailability { get; set; }
        public DbSet<CStudent> Students { get; set; }
        public DbSet<CLessonType> LessonType { get; set; }

        public DbSet<CDay> Days { get; set; }
        public DbSet<CStudioName> StudioNames { get; set; }
        public DbSet<CStudio> Studios { get; set; }
        public DbSet<CLesson> Lessons { get; set; }
        
        public CContext()
            : base("AzureConnection")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           // base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // CBusinessSettings
            modelBuilder.Entity<CBusinessSettings>().Property(x => x.strBusinessName).HasMaxLength(75);
           
            // COperationsLength
        
            // CStudioName
            modelBuilder.Entity<CStudioName>().Property(x => x.strStudioName).HasMaxLength(75);

            // CState
            modelBuilder.Entity<CState>().Property(x => x.strStateName).HasMaxLength(75);

            // CUserLayoutPreferences
            modelBuilder.Entity<CUserLayoutPreferences>().HasOptional(x => x.BackgroundImage).
                WithMany().HasForeignKey(x => x.CUserImageID).WillCascadeOnDelete(false);

            modelBuilder.Entity<CUserLayoutPreferences>().Property(x => x.strBackgroundColor).HasMaxLength(75);
            modelBuilder.Entity<CUserLayoutPreferences>().Property(x => x.strLayout).HasMaxLength(150).IsRequired();

            // CUserImages
            modelBuilder.Entity<CUserImage>().Property(x => x.strCaption).HasMaxLength(500);
            modelBuilder.Entity<CUserImage>().Property(x => x.bytImage).IsRequired();

            // CUserContent
            modelBuilder.Entity<CUserContent>().Property(x => x.strContentContainerID).HasMaxLength(150).IsRequired();

            // CInstructors
            modelBuilder.Entity<CInstructor>().HasRequired(x => x.State).
                WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<CInstructor>().Property(x => x.strFirstName).HasMaxLength(75).IsRequired();
            modelBuilder.Entity<CInstructor>().Property(x => x.strLastName).HasMaxLength(75).IsRequired();
            modelBuilder.Entity<CInstructor>().Property(x => x.strEmailAddress).HasMaxLength(150).IsRequired();
            modelBuilder.Entity<CInstructor>().Property(x => x.strPhoneNumber).HasMaxLength(75).IsRequired();
            modelBuilder.Entity<CInstructor>().Property(x => x.strAddress).HasMaxLength(150).IsRequired();
            modelBuilder.Entity<CInstructor>().Property(x => x.strCity).HasMaxLength(75).IsRequired();
            modelBuilder.Entity<CInstructor>().Property(x => x.strZipCode).HasMaxLength(75).IsRequired();

            // CStudents
            modelBuilder.Entity<CStudent>().HasRequired(x => x.State).
                WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<CStudent>().HasRequired(x => x.Instructor).
                WithMany(x => x.Students).HasForeignKey(x => x.CInstructorID).WillCascadeOnDelete(false);
            modelBuilder.Entity<CStudent>().Property(x => x.strFirstName).HasMaxLength(75).IsRequired();
            modelBuilder.Entity<CStudent>().Property(x => x.strLastName).HasMaxLength(75).IsRequired();
            modelBuilder.Entity<CStudent>().Property(x => x.strParentsFirstName).HasMaxLength(75);
            modelBuilder.Entity<CStudent>().Property(x => x.strEmailAddress).HasMaxLength(150).IsRequired();
            modelBuilder.Entity<CStudent>().Property(x => x.strPhoneNumber).HasMaxLength(75).IsRequired();
            modelBuilder.Entity<CStudent>().Property(x => x.strAddress).HasMaxLength(150).IsRequired();
            modelBuilder.Entity<CStudent>().Property(x => x.strCity).HasMaxLength(75).IsRequired();
            modelBuilder.Entity<CStudent>().Property(x => x.strZipCode).HasMaxLength(75).IsRequired();

            // CLessonTypes
            modelBuilder.Entity<CLessonType>().HasMany(x => x.Instructors).
                WithMany(x => x.LessonTypes).Map(
                            m =>
                            {
                                m.MapLeftKey("CLessonTypeID");
                                m.MapRightKey("CInstructorID");
                                m.ToTable("InstructorLessonTypes");
                            });
            modelBuilder.Entity<CLessonType>().Property(x => x.strLessonType).HasMaxLength(75).IsRequired();

            // CDays 
            

            // CLesson
            modelBuilder.Entity<CLesson>().Property(x => x.intStatus).IsRequired();

            // CStudio
        
        }
    }
}