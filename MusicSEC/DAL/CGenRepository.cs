﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CGenRepository<TEntity>
// Abstract : Generic Repository for Entities 
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Data.Entity;

namespace MusicSEC.DAL
{
    public class CGenRepository<TEntity> where TEntity : class
    {
        internal CContext context;
        internal DbSet<TEntity> dbSet;

        //---------------------------------------------------------------
        // Name: CGenRepository
        // Abstract: Constructor for CGenRepository  
        //---------------------------------------------------------------
        public CGenRepository(CContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        //---------------------------------------------------------------
        // Name: GetAll
        // Abstract: GetAll properties for model with filter order by and
        //           included properties
        //---------------------------------------------------------------
        public virtual IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> filter = null,
                                                   Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
                                                   string strIncludedProperties = "")
        {
            IQueryable<TEntity> iqQuery = dbSet;

            if (filter != null)
            {
                iqQuery = iqQuery.Where(filter);

            }

            foreach (var strProperty in strIncludedProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                iqQuery = iqQuery.Include(strProperty);
            }

            return (orderBy == null) ? iqQuery.ToList() : orderBy(iqQuery).ToList(); 
        }

        //---------------------------------------------------------------
        // Name: FindById
        // Abstract: Find entity by id   
        //---------------------------------------------------------------
        public virtual TEntity FindById(int id)
        {
            return dbSet.Find(id);
        }

        //---------------------------------------------------------------
        // Name: Insert
        // Abstract: CRUD Insert  
        //---------------------------------------------------------------
        public virtual void Insert(TEntity entity)
        {   
            dbSet.Add(entity); 
        }

        //---------------------------------------------------------------
        // Name: Remove
        // Abstract: CRUD Remove  
        //---------------------------------------------------------------
        public virtual void Remove(TEntity entity)
        {
            dbSet.Remove(entity);
        }

        //---------------------------------------------------------------
        // Name: Update
        // Abstract: CRUD Update  
        //---------------------------------------------------------------
        public virtual void Update(TEntity entity)
        {
            try
            {
                dbSet.Attach(entity);
                context.Entry(entity).State = System.Data.EntityState.Modified;
            }
            catch (Exception e)
            {
                string needtobelogged = e.ToString();
            }
        }

        //---------------------------------------------------------------
        // Name: Delete
        // Abstract: CRUD Find entity for Delete  
        //---------------------------------------------------------------
        public virtual void Delete(int id)
        {
            TEntity entity = dbSet.Find(id);
            Delete(entity);

        }

        //---------------------------------------------------------------
        // Name: CGenRepository
        // Abstract: CRUD Delete   
        //---------------------------------------------------------------
        public virtual void Delete(TEntity entity)
        {
            if (context.Entry(entity).State == System.Data.EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }
    }
}