﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class  : CDay
// Abstract : Database Model for Days
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MusicSEC.Models;

namespace MusicSEC.Models
{
    public class CDay
    {
        public int CDayID { get; set; }
        public DateTime dtmDate { get; set; }
        public int intOperationsStartTime { get; set; } // Minutes since midnight
        public int intOperationsEndTime { get; set; } // Minutes since midnight
        public bool blnOperational { get; set; }

        public virtual ICollection<CStudio> Studios { get; set; }
    }
}