﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CUserImages
// Abstract : Database Model for CUserImages
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.IO;
using MusicSEC.Utility;

namespace MusicSEC.Models
{
    public class CUserImage
    {
        public int CUserImageID { get; set; }
       
        public string strCaption { get; set; }
        public string strFileType { get; set; }
        public bool blnCaptionTop { get; set; }
        public byte[] bytImage { get; set; }

        public CUserImage ProcessBytes(HttpPostedFileBase file)
        {
            var image = new CUserImage();
            var extension = Path.GetFileName(file.FileName);

            if (extension != null)
                image.strFileType = extension.TrimStart('.');

            image.bytImage = file.ConvertImageToBytes();

            return image;
        }

        public byte[] ImageToBytes(HttpPostedFileBase file)
        {
            return file.ConvertImageToBytes();
        }

        public string GetFileName(HttpPostedFileBase file)
        {
            var extension = Path.GetFileName(file.FileName);
            if (extension != null)
                return extension.TrimStart('.');
            else
                return "";
        }

    }
}