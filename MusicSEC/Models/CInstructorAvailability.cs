﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class  : CInstructorAvailablity
// Abstract : Database Model for Intstructor Availablity
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MusicSEC.Models
{
    public class CInstructorAvailability
    {
        public int CInstructorAvailabilityID { get; set; }
        public int CInstructorID { get; set; }
        public int CDayID { get; set; }
        public int intStartTime { get; set; }
        public bool blnAvailable { get; set; }
    }
}