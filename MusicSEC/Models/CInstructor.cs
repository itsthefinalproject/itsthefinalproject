﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CInstructor
// Abstract : Database Model for CIntstructors
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace MusicSEC.Models
{
    public class CInstructor 
    {
        public int CInstructorID { get; set; }
        public int intUserID { get; set; }

        [Display(Name = "First Name")]
        public string strFirstName { get; set; }

        [Display(Name = "Last Name")]
        public string strLastName { get; set; }

        [Display(Name = "Email Address")]
        public string strEmailAddress { get; set; }

        [Display(Name = "Phone Number")]
        public string strPhoneNumber { get; set; }

        [Display(Name = "Address")]
        public string strAddress { get; set; }

        [Display(Name = "City")]
        public string strCity { get; set; }

        [Display(Name = "State")]
        public int CStateID { get; set; }

        [Display(Name = "Zip Code")]
        public string strZipCode { get; set; }

        [Display(Name = "About")]
        public string strProfileDescription { get; set; }

        public int? CUserImageID { get; set; }

        public bool blnActive { get; set; }

        //Navigations
        public virtual CState State { get; set; }
        public virtual CUserImage ProfileImage { get; set; }
        public virtual ICollection<CStudent> Students { get; set; }
        public virtual ICollection<CLessonType> LessonTypes { get; set; }
    }
}