﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CLessonTypes
// Abstract : Database Model for CLessonTypes
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace MusicSEC.Models
{
    public class CLessonType
    {
        public int CLessonTypeID { get; set; }
        [Display(Name = "Lesson Type")]
        public string strLessonType { get; set; }

        // Navigation
        public virtual ICollection<CInstructor> Instructors { get; set; }
    }
}