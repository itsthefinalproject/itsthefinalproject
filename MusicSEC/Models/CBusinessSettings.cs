﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CBusinessSettings
// Abstract : Database Model for Business Settings
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicSEC.Models
{
    public class CBusinessSettings
    {
        public int CBusinessSettingsID { get; set; }
        public string strBusinessName { get; set; }
        public string strTermsOfAgreement { get; set; }
        public int intLessonLengthInMins { get; set; }


        public virtual ICollection<COperationsLength> OperationsLength { get; set; }
    }
}