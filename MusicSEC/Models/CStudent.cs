﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CStudent
// Abstract : Database Model for CStudent
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using MusicSEC.Models;

namespace MusicSEC.Models
{
    public class CStudent
    {
        public int CStudentID { get; set; }
        public int CInstructorID { get; set; }
        public int intUserID { get; set; }

        [Display(Name = "First Name")]
        public string strFirstName { get; set; }

        [Display(Name = "Last Name")]
        public string strLastName { get; set; }

        [Display(Name = "Parents First Name")]
        public string strParentsFirstName { get; set; }

        [Display(Name = "Email Address")]
        public string strEmailAddress { get; set; }

        [Display(Name = "Phone Number")]
        public string strPhoneNumber { get; set; }

        [Display(Name = "Address")]
        public string strAddress { get; set; }

        [Display(Name = "City")]
        public string strCity { get; set; }

        [Display(Name = "State")]
        public int CStateID { get; set; }

        [Display(Name = "Zip Code")]
        public string strZipCode { get; set; }

        public bool blnActive { get; set; }
        public bool blnFirstTime { get; set; }

        // Navigation 
        public virtual CState State { get; set; }
        public virtual CInstructor Instructor { get; set; }
    }
}