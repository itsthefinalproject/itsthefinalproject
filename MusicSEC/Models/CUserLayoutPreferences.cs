﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CUserLayoutPreferences
// Abstract : Database Model for CUserLayoutPreferences
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MusicSEC.Models
{
    public class CUserLayoutPreferences
    {
        public int CUserLayoutPreferencesID { get; set; }
        public string strLayout { get; set; }
        public string strBackgroundColor { get; set; }
        public string strFontColor { get; set; }

        public int? CUserImageID { get; set; }

        // Navigation Properties
        public virtual CUserImage BackgroundImage { get; set; }
        public virtual ICollection<CUserContent> UserContent { get; set; }
    }
}