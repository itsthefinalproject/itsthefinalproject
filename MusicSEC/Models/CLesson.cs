﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CLesson
// Abstract : Database Model for Lessons
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MusicSEC.Models;

namespace MusicSEC.Models
{
    
    public class CLesson
    {
        public int CLessonID { get; set; }
        public int CStudentID { get; set; }
        public int CInstructorID { get; set; }
        public int intStartTime { get; set; } // Minutes since midnight
        public int intEndTime { get; set; } // Minutes since midnight
        public int CStudioID { get; set; }
        public int intStatus { get; set; }
        public string strInternalComments { get; set; }

        // Navigations
        public virtual CStudent Student { get; set; }
        public virtual CInstructor Instructor { get; set; }
        public virtual CStudio Studio { get; set; }

    }
}