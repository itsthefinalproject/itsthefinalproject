﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : COperationLength
// Abstract : Database Model for Operation Lengths
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicSEC.Models
{
    public class COperationsLength
    {
        public int COperationsLengthID { get; set; }
        public int intStartTime { get; set; }
        public int intEndTime { get; set; }
        public int intDayCode { get; set; } // 0 Sun, 1 Mon, 2 Tues, 3 Wed, 4 Thur, 5 Fri, 6 Saturday
        public int CBusinessSettingsID { get; set; }
        public bool blnIsOperational { get; set; }
    }
}