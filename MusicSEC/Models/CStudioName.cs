﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CStudioName
// Abstract : Database Model for Studio Names
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MusicSEC.Models
{
    public class CStudioName
    {
        public int CStudioNameID { get; set; }

        [Display(Name = "Studio Name")]
        public string strStudioName { get; set; }
    }
}