﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CState
// Abstract : Database Model for States
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MusicSEC.Models
{
    public class CState
    {
        public int CStateID { get; set; }
        public string strStateName { get; set; }

        public CState()
        { }

        public CState(string stateName)
        {
            strStateName = stateName;
        }
    }
}