﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CUserContent
// Abstract : Database Model for CUserContent
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MusicSEC.Models
{
    public class CUserContent
    {
        public int CUserContentID { get; set; }
        public string strContentContainerID { get; set; }

        public string strContentHTML { get; set; }
        public string strBackgroundColor { get; set; }
        public int CUserLayoutPreferencesID { get; set; }
        public int? CUserImageID { get; set; }
        public bool blnIsBackgroundImage { get; set; }

        // Navigation Properties
        public virtual CUserImage Image { get; set; }
        public virtual CUserLayoutPreferences CUserLayoutPreferences { get; set; }
    }
}