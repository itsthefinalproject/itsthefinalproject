﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CStudio
// Abstract : Database Model for Studios
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MusicSEC.Models
{
    public class CStudio
    {
        public int CStudioID { get; set; }
        public int CStudioNameID { get; set; }
        public int CDayID { get; set; }
        public int intLessonSlotsPerDay { get; set; }

        // Navigation
        public virtual CStudioName StudioName { get; set; }
        public virtual CDay Day { get; set; }
        public virtual ICollection<CLesson> StudioLessons { get; set; }
    }
}