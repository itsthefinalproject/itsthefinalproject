﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CSeedUtiliy
// Abstract : Utility functions for seeding various system tables
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;
using MusicSEC.DAL;
using MusicSEC.Models;

namespace MusicSEC.Utility
{
    public static class CSeedUtility
    {
        //--------------------------------------------------------------
        // Name : SeedRoles
        // Abstract : seed potential roles for the application
        //--------------------------------------------------------------
        public static void SeedRoles()
        {
            WebSecurity.InitializeDatabaseConnection("AzureConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);
            var roles = (SimpleRoleProvider) Roles.Provider;
            var membership = (SimpleMembershipProvider) Membership.Provider;

            if (!roles.RoleExists("Administrator"))
                roles.CreateRole("Administrator");

            if (!roles.RoleExists("Instructor"))
                roles.CreateRole("Instructor");

            if (!roles.RoleExists("EnrolledStudent"))
                roles.CreateRole("EnrolledStudent");

            if (!roles.RoleExists("PotentialStudent"))
                roles.CreateRole("PotentialStudent");
        }

        //--------------------------------------------------------------
        // Name : SeedStates
        // Abstract : create a list of states for a drop down list
        //--------------------------------------------------------------
        public static void SeedStates()
        {
            string strStates = "Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware, " +
                               "District of Columbia, Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, " +
                               "Kentucky, Louisiana, Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, " +
                               "Missouri, Montana, Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, " +
                               "North Carolina, North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, " +
                               "South Carolina, South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, " +
                               "West Virginia, Wisconsin, Wyoming";

            string[] astrStates = strStates.Split(new char[] { ',' });
            
            using (CUnitOfWork unit = new CUnitOfWork())
            {
                foreach (string state in astrStates)
                {
                    if(!unit.StateRep.GetAll().Any(x => x.strStateName.Trim().ToUpper() == state.Trim().ToUpper()))
                    {
                        unit.StateRep.Insert(new CState(state));
                    }
                }

                unit.Save();
            }
        }

        //--------------------------------------------------------------
        // Name : SeedAdministrator
        // Abstract : seed the initial administrator role
        //--------------------------------------------------------------
        public static void SeedAdministrator()
        {
            RegisterModel model = new RegisterModel();
            model.UserName = "Admin";
            model.Password = "password";

            if (!WebSecurity.UserExists(model.UserName))
            {
                WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
            }

            if (!Roles.GetRolesForUser("Admin").Contains("Administrator"))
                Roles.AddUserToRole("Admin", "Administrator");

            // This works checking statement above
            // Roles.AddUsersToRoles(new[] { "Admin" }, new[] { "Administrator" });

        }

        //--------------------------------------------------------------
        // Name : SeedBusinessSettings
        // Abstract : seed the initial business settings
        //--------------------------------------------------------------
        public static void SeedBusinessSettings()
        {
            using (CUnitOfWork unit = new CUnitOfWork())
            {
                if (unit.BusinessSettingsRep.GetAll().Count() > 0)
                    return;

                var businessSettings = new CBusinessSettings();
                businessSettings.strBusinessName = "";
                businessSettings.intLessonLengthInMins = 30;
                businessSettings.OperationsLength = new List<COperationsLength>();

                for (int i = 0; i < 7; i++)
                {
                    var operationsLength = new COperationsLength();
                    operationsLength.intDayCode = i;
                    operationsLength.intStartTime = 540;
                    operationsLength.intEndTime = 1020;
                    operationsLength.blnIsOperational = (i == 0 || i == 6) ? false : true;
                    businessSettings.OperationsLength.Add(operationsLength);
                }

                unit.BusinessSettingsRep.Insert(businessSettings);
                unit.Save();
            }
        }

        //--------------------------------------------------------------
        // Name : SeedNewInstructor
        // Abstract : creates a user name and password for instructor creation
        //--------------------------------------------------------------
        public static string[] SeedNewInstructor(string businessName, string email = "")
        {
            CPasswordUtility passwordUtility = new CPasswordUtility();
            string strUserName = passwordUtility.CreateRandomUserName();
            string strPassword = "";
            string[] astrInstructorInfo = new string[2];
            bool blnSuccess = true;
            int intCnt = 0;
            if (email == "")
            {
                while (WebSecurity.UserExists(strUserName))
                {
                    strUserName = passwordUtility.CreateRandomUserName();
                    if (intCnt > 10)
                    {
                        blnSuccess = false;
                        strUserName = "";
                        break;
                    }
                    intCnt++;
                }
            }
            else
            {
                strUserName = email;
            }
            if (blnSuccess)
            {
                strPassword = passwordUtility.CreateRandomPassword();
                WebSecurity.CreateUserAndAccount(strUserName, strPassword);

                if (!Roles.GetRolesForUser(strUserName).Contains("Instructor"))
                    Roles.AddUserToRole(strUserName, "Instructor");

                astrInstructorInfo[0] = strUserName;
                astrInstructorInfo[1] = strPassword;
            }

            return astrInstructorInfo;
        }

        //--------------------------------------------------------------
        // Name : SeedCalendar
        // Abstract : seed the calendar for a year with business settings settings
        //--------------------------------------------------------------
        public static bool SeedCalendar(CUnitOfWork unit, DateTime startDate)
        {
            bool blnSuccess = true;
            int intDayIndex = (int)startDate.DayOfWeek;
            int intDayCnt = 365;


            var businessSettings = unit.BusinessSettingsRep.GetAll().FirstOrDefault();
            var studioNames = unit.StudioNameRep.GetAll().ToList();

            if (DateTime.IsLeapYear(startDate.Year))
            {
                intDayCnt = 366;
            }
            int intCnt = 0;
            for (int i = intDayIndex; i < intDayCnt + intDayIndex; i++)
            {
                var operationsLength = businessSettings.OperationsLength.FirstOrDefault(x => x.intDayCode == i % 7);
                var day = new CDay();
                int intLessonsPerDay = CTimeUtility.CalculateLessonsPerDay(operationsLength.intStartTime, operationsLength.intEndTime, businessSettings.intLessonLengthInMins);
                day.dtmDate = startDate.AddDays(intCnt);
                day.blnOperational = operationsLength.blnIsOperational;
                day.intOperationsStartTime = operationsLength.intStartTime;
                day.intOperationsEndTime = operationsLength.intEndTime;
                day.Studios = new List<CStudio>();

                for (int n = 0; n < studioNames.Count; n++)
                {
                    var studio = new CStudio();
                    studio.CStudioNameID = studioNames[n].CStudioNameID;
                    studio.intLessonSlotsPerDay = intLessonsPerDay;
                    day.Studios.Add(studio);
                }

                unit.DayRep.Insert(day);
                intCnt++;
            }
            unit.Save();
            return blnSuccess;
        }
    }
}