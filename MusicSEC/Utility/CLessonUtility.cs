﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CLesson Utility
// Abstract : Utility functions for lessons
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MusicSEC.DAL;
using MusicSEC.Models;
using MusicSEC.ViewModels;

namespace MusicSEC.Utility
{
    public class CLessonUtility
    {
        private static CLessonUtility instance;

        // Singleton
        public static CLessonUtility Instance
        {
            get { return instance ?? new CLessonUtility(); }
        }

        //--------------------------------------------------------------
        // Name : LessonByStatus
        // Abstract : return list of lessons by status and by instructor
        //--------------------------------------------------------------
        public List<CLessonVM> LessonsByStatus(CUnitOfWork unit, int instructorId, int statusId, int daysOut)
        {
            var lessons = unit.LessonRep.GetAll(x => x.CInstructorID == instructorId && x.intStatus == statusId).ToList();
            var lessonVms = new List<CLessonVM>();
            int intDaysOut = (statusId == 0 || statusId == 1 || statusId == 3) ? daysOut * -1 : daysOut;
            DateTime dtmNow = DateTime.Now.AddDays(-1).Date;
            DateTime dtmDate = DateTime.Now.AddDays(intDaysOut).Date;
            foreach (var lesson in lessons)
            {
                var studio = unit.StudioRep.FindById(lesson.CStudioID);
                var day = unit.DayRep.FindById(studio.CDayID);
      
                if (day.dtmDate.Date >= dtmNow && day.dtmDate.Date <= dtmDate && intDaysOut > 0)
                {
                    var lessonVm = new CLessonVM().MapViewModel(lesson, unit);
                    lessonVms.Add(lessonVm);
                }
                else if (day.dtmDate.Date >= dtmDate && day.dtmDate.Date <= dtmNow && intDaysOut < 0)
                {
                    var lessonVm = new CLessonVM().MapViewModel(lesson, unit);
                    lessonVms.Add(lessonVm);
                }
            }

            return lessonVms;
        }

        //--------------------------------------------------------------
        // Name : InstructorReview
        // Abstract : return a list of lessons per instructor for review
        //--------------------------------------------------------------
        public List<CLessonVM> InstructorReview(CUnitOfWork unit, int instructorId)
        {
            var lessons = unit.LessonRep.GetAll(x => x.CInstructorID == instructorId && x.intStatus != 0).ToList();
            var lessonVms = new List<CLessonVM>();
            foreach(var lesson in lessons)
            {
                var studio = unit.StudioRep.FindById(lesson.CStudioID);
                var day = unit.DayRep.FindById(studio.CDayID);
                if(day.dtmDate <= DateTime.Now && lesson.intStatus != 0 && lesson.intStatus != 1)
                {
                    var lessonVm = new CLessonVM().MapViewModel(lesson, unit);
                    lessonVms.Add(lessonVm);
                }
            }
            return lessonVms;
        }

        //--------------------------------------------------------------
        // Name : InstructorLessons
        // Abstract : get lessons for instructor
        //--------------------------------------------------------------
        public List<CLessonVM> InstructorLessons(CUnitOfWork unit, int instructorId, int daysOut)
        {          
            var lessons = unit.LessonRep.GetAll(x => x.CInstructorID == instructorId).ToList();
            var lessonVms = new List<CLessonVM>();
            DateTime dtmNow = DateTime.Now.AddDays(-1);
            DateTime dtmDate = DateTime.Now.AddDays(daysOut);
            foreach (var lesson in lessons)
            {
                var studio = unit.StudioRep.FindById(lesson.CStudioID);
                var day = unit.DayRep.FindById(studio.CDayID);
      
                if (day.dtmDate >= dtmNow && day.dtmDate <= dtmDate)
                {
                    var lessonVm = new CLessonVM().MapViewModel(lesson, unit);
                    lessonVms.Add(lessonVm);
                }
            }

            return lessonVms;
        }

        //--------------------------------------------------------------
        // Name : FirstTimeLessons
        // Abstract : return list of first time lessons for instructor
        //--------------------------------------------------------------
        public List<CLessonVM> FirstTimeLessons(CUnitOfWork unit, int instructorId, int daysOut)
        {
            var lessons = unit.LessonRep.GetAll(x => x.CInstructorID == instructorId && x.intStatus == 2).ToList();
            var lessonVms = new List<CLessonVM>();
            DateTime dtmNow = DateTime.Now.AddDays(-1);
            DateTime dtmDate = DateTime.Now.AddDays(daysOut);
            foreach (var lesson in lessons)
            {
                var studio = unit.StudioRep.FindById(lesson.CStudioID);
                var day = unit.DayRep.FindById(studio.CDayID);

                if (day.dtmDate >= dtmNow && day.dtmDate <= dtmDate)
                {
                    var lessonVm = new CLessonVM().MapViewModel(lesson, unit);
                    lessonVms.Add(lessonVm);
                }
            }

            return lessonVms;
        }

        //--------------------------------------------------------------
        // Name : StudentLessons
        // Abstract : return list of lessons per student
        //--------------------------------------------------------------
        public List<CLessonVM> StudentLessons(CUnitOfWork unit, int studentId, int daysOut)
        {
            var lessons = unit.LessonRep.GetAll(x => x.CStudentID == studentId && x.intStatus == 2 || x.CStudentID == studentId && x.intStatus == 4).ToList();
            var lessonVms = new List<CLessonVM>();
            DateTime dtmNow = DateTime.Now.AddDays(-1);
            DateTime dtmDate = DateTime.Now.AddDays(daysOut);
            foreach (var lesson in lessons)
            {
                var studio = unit.StudioRep.FindById(lesson.CStudioID);
                var day = unit.DayRep.FindById(studio.CDayID);

                if (day.dtmDate >= dtmNow && day.dtmDate <= dtmDate)
                {
                    var lessonVm = new CLessonVM().MapViewModel(lesson, unit);
                    lessonVms.Add(lessonVm);
                }
            }

            return lessonVms;
        }

        //--------------------------------------------------------------
        // Name : AvailLessonsByDate
        // Abstract : retrun list of lessons slots that are available by date
        //--------------------------------------------------------------
        public List<CLessonVM> AvailLessonsByDate(CUnitOfWork unit, DateTime date, int instructorId)
        {
            List<CLessonVM> lstVMs = new List<CLessonVM>();

            var studios = unit.DayRep.GetAll(x => x.dtmDate.Day == date.Day && x.dtmDate.Month == date.Month && x.dtmDate.Year == date.Year).SelectMany(x => x.Studios).ToList();
            foreach (var studio in studios)
            {
                var availableTimes = unit.InstructorAvailRep.GetAll(x => x.CInstructorID == instructorId && x.CDayID == studio.CDayID && x.blnAvailable == true).ToList();
                CStudioVM studioVm = new CStudioVM().MapViewModel(studio, unit);
                foreach(var lessonVm in studioVm.dctLessonSlots.Values)
                {
                    bool blnAdd = false;
                    if (availableTimes != null && availableTimes.Count > 0)
                    {
                        if (lessonVm.blnAvailable && availableTimes.Any(x => x.intStartTime == lessonVm.intStartTime))
                        {
                            blnAdd = true;
                        }
                    }
                    else 
                    {
                        if (lessonVm.blnAvailable)
                        {
                            blnAdd = true;
                        }
                    }

                    blnAdd = availableTimes.Any(x => x.intStartTime == lessonVm.intStartTime && x.CDayID == studio.CDayID); 

                    if (blnAdd && !lstVMs.Exists(x => x.intStartTime == lessonVm.intStartTime))
                    {
                        if(lessonVm.CInstructorID == 0 || lessonVm.CInstructorID == instructorId)
                            lstVMs.Add(lessonVm);
                    }
                }
            }
            return lstVMs;
        }

        //--------------------------------------------------------------
        // Name : AvailLessonsByDateRange
        // Abstract : retrun list of lessons slots that are available by date range
        //--------------------------------------------------------------
        public List<CLessonVM> AvailLessonsByDateRange(CUnitOfWork unit, DateTime date1, DateTime date2, int instructorId)
        {
            List<CLessonVM> lstVms = new List<CLessonVM>();
            //var studios = unit.DayRep.GetAll(x => x.dtmDate.Day >= date1.Day && x.dtmDate.Month >= date1.Month && x.dtmDate.Year >= date1.Year &&
            //                                      x.dtmDate.Day <= date2.Day && x.dtmDate.Month <= date2.Month && x.dtmDate.Year <= date2.Year)
            //                          .SelectMany(x => x.Studios).ToList();

            var studios = unit.DayRep.GetAll(x => x.dtmDate >= date1.Date && x.dtmDate <= date2.Date).SelectMany(x => x.Studios).ToList(); 
            foreach (var studio in studios)
            {
                var availableTimes = unit.InstructorAvailRep.GetAll(x => x.CInstructorID == instructorId && x.CDayID == studio.CDayID && x.blnAvailable == true).ToList();
                CStudioVM studioVm = new CStudioVM().MapViewModel(studio, unit);
                foreach (var lessonVm in studioVm.dctLessonSlots.Values)
                {
                    bool blnAdd = false;
                    var day = unit.DayRep.FindById(studio.CDayID);
                    lessonVm.dtmDate = day.dtmDate;
                    if (availableTimes != null && availableTimes.Count > 0)
                    {
                        if (lessonVm.blnAvailable && availableTimes.Any(x => x.intStartTime == lessonVm.intStartTime))
                        {
                            blnAdd = true;
                        }
                    }
                    else
                    {
                        if (lessonVm.blnAvailable)
                        {
                            blnAdd = true;
                        }
                    }

                    blnAdd = availableTimes.Any(x => x.intStartTime == lessonVm.intStartTime && x.CDayID == studio.CDayID);
                    if (blnAdd && !lstVms.Exists(x => x.intStartTime == lessonVm.intStartTime && x.dtmDate.Day == day.dtmDate.Day && x.dtmDate.Month == day.dtmDate.Month && x.dtmDate.Year == day.dtmDate.Year ))
                    {
                        if (lessonVm.CInstructorID == 0 || lessonVm.CInstructorID == instructorId)
                            lstVms.Add(lessonVm);
                    }
                }
            }
            return lstVms;
        }

        //--------------------------------------------------------------
        // Name : AvailLessonsByInstructor
        // Abstract : retrun list of lessons slots that are available by instructor
        //--------------------------------------------------------------
        public List<CLessonVM> AvailLessonsByInstructor(CUnitOfWork unit, int instructorId, int daysOut)
        {
            List<CLessonVM> lstVms = new List<CLessonVM>();
            DateTime dtmDate = DateTime.Now;
            DateTime dtmDateOut = DateTime.Now.AddDays(daysOut);
            var studios = unit.DayRep.GetAll(x => x.dtmDate.Day >= dtmDate.Day && x.dtmDate.Month >= dtmDate.Month && x.dtmDate.Year >= dtmDate.Year &&
                                                  x.dtmDate.Day <= dtmDateOut.Day && x.dtmDate.Month <= dtmDateOut.Month && x.dtmDate.Year <= dtmDateOut.Year)
                                      .SelectMany(x => x.Studios).ToList();
            foreach (var studio in studios)
            {
                var availableTimes = unit.InstructorAvailRep.GetAll(x => x.CInstructorID == instructorId && x.CDayID == studio.CDayID && x.blnAvailable == true).ToList();
                CStudioVM studioVm = new CStudioVM().MapViewModel(studio, unit);
                foreach (var lessonVm in studioVm.dctLessonSlots.Values)
                {
                    bool blnAdd = false;
                    var day = unit.DayRep.FindById(studio.CDayID);
                    lessonVm.dtmDate = day.dtmDate;
                    if (availableTimes != null && availableTimes.Count > 0)
                    {
                        if (lessonVm.blnAvailable && availableTimes.Any(x => x.intStartTime == lessonVm.intStartTime))
                        {
                            blnAdd = true;
                        }
                    }
                    else
                    {
                        if (lessonVm.blnAvailable)
                        {
                            blnAdd = true;
                        }
                    }

                    blnAdd = availableTimes.Any(x => x.intStartTime == lessonVm.intStartTime && x.CDayID == studio.CDayID);
                    if (blnAdd && !lstVms.Exists(x => x.intStartTime == lessonVm.intStartTime && x.dtmDate.Day == day.dtmDate.Day && x.dtmDate.Month == day.dtmDate.Month && x.dtmDate.Year == day.dtmDate.Year))
                    {
                        lstVms.Add(lessonVm);
                    }
                }
            }
            return lstVms;
        }

        //--------------------------------------------------------------
        // Name : CreateLesson
        // Abstract : Utiltiy function for quick create of lesson
        //--------------------------------------------------------------
        public CLesson CreateLesson(CUnitOfWork unit, int studioId, int startTime, int studentId, int instructorId)
        {
            int intLessonLength = unit.BusinessSettingsRep.GetAll().Select(x => x.intLessonLengthInMins).FirstOrDefault();
            var lesson = new CLesson();
            lesson.CStudioID = studioId;
            lesson.CStudentID = studentId;
            lesson.CInstructorID = instructorId;
            lesson.intStartTime = startTime;
            lesson.intEndTime = startTime + intLessonLength;
            lesson.intStatus = (unit.LessonRep.GetAll().Any(x => x.CStudentID == studentId)) ? 4 : 2; // 4 : Schedule || 2 : FirstLesson  

            return lesson;
        }

        //--------------------------------------------------------------
        // Name : StatByStatus
        // Abstract : return instructor id with most or least amount of lessons by status
        //--------------------------------------------------------------
        public int StatByStatus(CUnitOfWork unit, int statusId, bool most, DateTime start, DateTime end)
        {
            int intInstructorId = 0;
            var lessons = unit.LessonRep.GetAll(x => x.intStatus == statusId && 
                                                x.Studio.Day.dtmDate >= start.Date && 
                                                x.Studio.Day.dtmDate <= end.Date).ToList();

            if (lessons.Count == 0)
                return intInstructorId;

            var instructors = unit.InstructorRep.GetAll(x => x.blnActive).ToList();

            if (instructors != null && instructors.Count > 0)
            {
                var leadInstructor = instructors[0]; 
                foreach (var instructor in instructors)
                {
                    if (most)
                    {
                        if (lessons.Where(x => x.CInstructorID == instructor.CInstructorID).Count() > lessons.Where(x => x.CInstructorID == leadInstructor.CInstructorID).Count())
                        {
                            leadInstructor = instructor;
                        }
                    }
                    else
                    {
                        if (lessons.Where(x => x.CInstructorID == instructor.CInstructorID).Count() < lessons.Where(x => x.CInstructorID == leadInstructor.CInstructorID).Count())
                        {
                            leadInstructor = instructor;
                        }
                    }
                }
                intInstructorId = leadInstructor.CInstructorID;
            }

            return intInstructorId;
        }

        public List<CLessonVM> FilterBookedLessons(CUnitOfWork unit, int instructorId, List<CLessonVM> lessons)
        {
            return lessons;
        }

        //--------------------------------------------------------------
        // Name : LessonStatus
        // Abstract : return dictionary of lesson statuses to be used for select list
        //--------------------------------------------------------------
        public Dictionary<int, string> LessonStatus()
        {
            string[] astrValues = Enum.GetNames(typeof(LessonStatus));
            var dctLessonStatus = new Dictionary<int, string>();

            for (int i = 0; i < astrValues.Length; i++)
            {
                string strValue = astrValues[i];
                strValue = strValue.Replace("_", " ");
                dctLessonStatus.Add(i, strValue);
            }
            return dctLessonStatus;
        }

        //--------------------------------------------------------------
        // Name : DaysOut
        // Abstract : return dictionary of days incremented by seven for select list
        //--------------------------------------------------------------
        public Dictionary<int, string> DaysOut()
        {
            var dctDaysOut = new Dictionary<int, string>();
            for (int i = 7; i < 56; i += 7)
            {
                dctDaysOut.Add(i, i.ToString());
            }
            return dctDaysOut;
        }

        //--------------------------------------------------------------
        // Name : LessonOrder
        // Abstract : return dictionary of lesson order options for select list
        //--------------------------------------------------------------
        public Dictionary<int, string> LessonOrder()
        {
            var dctLessonOrder = new Dictionary<int, string>();
            dctLessonOrder.Add(1, "Most");
            dctLessonOrder.Add(2, "Least");
            return dctLessonOrder;
        }
    }
}