﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CEmail Utility
// Abstract : Utility functions for sending emails
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using MusicSEC.DAL;

namespace MusicSEC.Utility
{
    public static class CEmailUtility
    {
        //-------------------------------------------------------------------------
        //  Name: Send Email
        //  Abstract: Send an Email to a new instructor with account info
        //-------------------------------------------------------------------------
        public static bool SendEmail(string emailAddress, string userName, string password, string businessName)
        {
            bool blnSuccess = true;
            string strFileName = HttpContext.Current.Server.MapPath("~/Utility/ContactForm.txt");
            string strMailBody = File.ReadAllText(strFileName);

            MailMessage message = new MailMessage();
            message.To.Add(new MailAddress(emailAddress, emailAddress));
            message.From = new MailAddress("musicsec@yahoo.com", "DO-NOT-REPLY");
            message.Subject = businessName + " Instructor invitation";

            strMailBody = strMailBody.Replace("##BusinessName##", businessName);
            strMailBody = strMailBody.Replace("##UserName##", userName);
            strMailBody = strMailBody.Replace("##Password##", password);
           
            message.Body = strMailBody;

            SmtpClient client = new SmtpClient("smtp.mail.yahoo.com");
            client.Port = 587;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("musicsec@yahoo.com", "Azygous420");
            client.EnableSsl = true;

            try
            {
                client.Send(message);
            }
            catch
            {
                blnSuccess = false;
            }

            return blnSuccess;
        }

        //-------------------------------------------------------------------------
        //  Name: EmailStudent
        //  Abstract: Email a student
        //-------------------------------------------------------------------------
        public static bool EmailStudent(string subject, string body, string emailAddress)
        {
            bool blnSuccess = true;
            MailMessage message = new MailMessage();
            message.To.Add(new MailAddress(emailAddress, emailAddress));
            message.From = new MailAddress("musicsec@yahoo.com", "DO-NOT-REPLY");
            message.Subject = subject;

            message.Body = body;

            SmtpClient client = new SmtpClient("smtp.mail.yahoo.com");
            client.Port = 587;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("musicsec@yahoo.com", "Azygous420");
            client.EnableSsl = true;

            try
            {
                client.Send(message);
            }
            catch
            {
                blnSuccess = false;
            }

            return blnSuccess;
        }
    }
}