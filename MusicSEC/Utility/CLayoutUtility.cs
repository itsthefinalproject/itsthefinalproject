﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CLayoutUtility
// Abstract : Utility functions for the layout editor
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using MusicSEC.DAL;

namespace MusicSEC.Utility
{
    public static class CLayoutUtility
    {
        private static Dictionary<string, string> dctLayoutDict;
        private static string strCurrentLayout;
        private static string strBackgroundColor;
        private static string strBackgroundImgSrc;
        private static string strFontColor;
        private static int intBackgroundImgId;

        // Constructor
        static CLayoutUtility()
        {
            strCurrentLayout = "";
            strBackgroundImgSrc = "";
            strFontColor = "";
            strBackgroundColor = "";
        }

        // property get business name
        public static string BusinessName
        {
            get
            {
                using (CUnitOfWork unit = new CUnitOfWork())
                {
                    return unit.BusinessSettingsRep.GetAll().Select(x => x.strBusinessName).FirstOrDefault();
                }
            }
        }

        // property get the layout dictionary
        public static Dictionary<string, string> LayoutDict
        {
            get
            {
                if(dctLayoutDict == null)
                    InitLayoutDict();

                return dctLayoutDict;
            }
        }

        // property get current layout
        public static string CurrentLayout
        {
            get
            {
                if (strCurrentLayout == null || strCurrentLayout == "")
                {
                    using (CUnitOfWork unit = new CUnitOfWork())
                    {
                        strCurrentLayout = unit.LayoutPrefsRep.GetAll().Select(x => x.strLayout).FirstOrDefault();
                        if (strCurrentLayout == null || strCurrentLayout == "")
                            strCurrentLayout = LayoutDict.Values.ToList().FirstOrDefault();
                    }
                }
                return strCurrentLayout;
            }
            set { strCurrentLayout = value; }
        }

        // Property get Background color
        public static string BackgroundColor
        {
            get
            {
                if (strBackgroundColor == null || strBackgroundColor == "")
                {
                    using (CUnitOfWork unit = new CUnitOfWork())
                    {
                        strBackgroundColor = unit.LayoutPrefsRep.GetAll().Select(x => x.strBackgroundColor).FirstOrDefault();
                        if (strBackgroundColor == null || strBackgroundColor == "")
                            strBackgroundColor = "#FFFFFF";

                    }
                }
                return strBackgroundColor;
            }
            set { strBackgroundColor = value; }
        }

        // property get font color
        public static string FontColor
        {
            get
            {
                if (strFontColor == null || strFontColor == "")
                {
                    using (CUnitOfWork unit = new CUnitOfWork())
                    {
                        strFontColor = unit.LayoutPrefsRep.GetAll().Select(x => x.strFontColor).FirstOrDefault();
                        if (strFontColor == null || strFontColor == "")
                            strFontColor = "#000000";

                    }
                }
                return strFontColor;
            }
            set { strFontColor = value; }
        }

        // Property Set background img src
        public static string SetBackgroundImgSrc
        {
            set { strBackgroundImgSrc = value; }
        }

        //--------------------------------------------------------------
        // Name : BackgroundImgSrc
        // Abstract : return the img src for the main background image
        //--------------------------------------------------------------
        public static string BackgroundImgSrc()
        {
            string strSrc = "";
            using (CUnitOfWork unit = new CUnitOfWork())
            {
                int intImgId = unit.LayoutPrefsRep.GetAll().Select(x => x.CUserImageID).FirstOrDefault() ?? -1;
                if (strBackgroundImgSrc == null || strBackgroundImgSrc == "" || intBackgroundImgId != intImgId)
                {
                    if (intImgId != -1)
                    {
                        var image = unit.ImageRep.FindById(intImgId);
                        strSrc = string.Format("data:image/{0}; base64, {1}", image.strFileType, Convert.ToBase64String(image.bytImage));
                        strBackgroundImgSrc = strSrc;
                        intBackgroundImgId = intImgId;
                    }
                    else
                    {
                        strBackgroundImgSrc = "";
                    }
                }
            }
           
            return strBackgroundImgSrc;
           
        }

        //--------------------------------------------------------------
        // Name : GenerateImgSrc
        // Abstract : generate source for images stored in db
        //--------------------------------------------------------------
        public static string GenerateImgSrc(int imageId)
        {
            using (CUnitOfWork unit = new CUnitOfWork())
            {
                var image = unit.ImageRep.FindById(imageId);
                string strSrc = "";
                if (image != null)
                {
                    strSrc = string.Format("data:image/{0}; base64, {1}", image.strFileType, Convert.ToBase64String(image.bytImage));
                }
                return strSrc;
            }
        }

        //--------------------------------------------------------------
        // Name : InitLayoutDict
        // Abstract : get all layouts in folder and add them to be 
        //            options in layout settings
        //--------------------------------------------------------------
        private static void InitLayoutDict()
        {
            dctLayoutDict = new Dictionary<string, string>();
            DirectoryInfo directory = new DirectoryInfo(HttpContext.Current.Server.MapPath(@"~/Views/Shared"));
            var files = directory.GetFiles().Where(x => x.Name.Trim().ToUpper().Contains("_LAYOUT")).Select(x => x.Name).ToList();
            for (int i = 0; i < files.Count; i++)
            {
                dctLayoutDict.Add("Layout " + (i + 1).ToString(), files[i]);
            }
        }
        
    }
}