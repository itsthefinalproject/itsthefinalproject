﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CTimeUtility
// Abstract : Utility functions to handle time functions
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicSEC.Utility
{
   

    public static class CTimeUtility
    {
        //--------------------------------------------------------------
        // Name : MinutesSinceMidnight
        // Abstract : convert readable time to minutes since midnight
        //--------------------------------------------------------------
        public static int MinutesSinceMidnight(int hours, int min, bool isAm)
        {
            hours = (isAm && hours == 12) ? hours * 0 : hours;
            int intMinSinceMidnight = (isAm) ? hours * 60 : (hours + 12) * 60;
            intMinSinceMidnight += min;
            return intMinSinceMidnight;
        }

        //--------------------------------------------------------------
        // Name : ConvertHours
        // Abstract : convert minutes since midnight hours
        //--------------------------------------------------------------
        public static int ConvertHours(int min)
        {
            decimal dcmMinutes = Convert.ToDecimal(min);
            int intHours = (int)Math.Floor(dcmMinutes / 60);
            return (intHours >= 12) ? intHours - 12 : intHours;
        }

        //--------------------------------------------------------------
        // Name : ConvertMinutes
        // Abstract : convert minutes since midnight minutes
        //--------------------------------------------------------------
        public static int ConvertMinutes(int min)
        {
            int intHours = ConvertHours(min);
            intHours = (min >= 720) ? intHours + 12 : intHours;

            int m = min - (intHours * 60);
            return min - (intHours * 60);
        }

        //--------------------------------------------------------------
        // Name : IsAm
        // Abstract : use minutes to figure out if this is am or pm
        //--------------------------------------------------------------
        public static int IsAm(int min)
        {
            int b = (min < 720) ? 0 : 1;
            return (min < 720) ? 0 : 1;
        }

        //--------------------------------------------------------------
        // Name : ConvertToReadable
        // Abstract : convert minutes since midnight to a readable format
        //--------------------------------------------------------------
        public static string ConvertToReadable(int min)
        {
            int intHours = ConvertHours(min);
            string strHours = (intHours == 0) ? "12" : intHours.ToString();

            int intMins = ConvertMinutes(min);
            string strMins = intMins.ToString();

            if (strMins.Length == 1)
                strMins = strMins.PadRight(2, '0');

            string strAmPm = (min >= 720) ? "PM" : "AM";

            return strHours + " : " + strMins + " " + strAmPm;
        }

        //--------------------------------------------------------------
        // Name : CalculateLessonsPerDay
        // Abstract : calculate how many lessons per day
        //--------------------------------------------------------------
        public static int CalculateLessonsPerDay(int startTime, int endTime, int lessonLength)
        {
            return (endTime - startTime) / lessonLength;
        }

    }
}