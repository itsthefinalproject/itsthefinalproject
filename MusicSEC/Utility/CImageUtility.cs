﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CImageUtility
// Abstract : Utiltiy to turn images into byte arrays for storage in database
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web.Configuration;

namespace MusicSEC.Utility
{
    public static class CImageUtility
    {
        private const int DEFAULT_IMAGE_WIDTH = 1000;
        private const int DEFAULT_IMAGE_HEIGHT = 1000;
        //---------------------------------------------------------------
        // Name : IsResizeNeeded
        // Abstract : Check if image needs to be resized
        //---------------------------------------------------------------
        private static bool IsResizeNeeded(Image image, int height, int width)
        {
            var originalWidth = image.Width;
            var originalHeight = image.Height;

          //  return (originalHeight != height) || (originalWidth != width);
            return (originalHeight > height) || (originalWidth > width);
        }

        //---------------------------------------------------------------
        // Name : ResizeBySize
        // Abstract : Resize the image
        //---------------------------------------------------------------
        private static Image ResizeBySize(Image image, int height, int width)
        {
            var originalWidth = image.Width;
            var originalHeight = image.Height;

            var modifiedHeight = height;
            var modifiedWidth = width;

            var bitmap = new Bitmap(modifiedHeight, modifiedHeight, PixelFormat.Format32bppArgb);
            bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            var graphics = Graphics.FromImage(bitmap);
            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphics.DrawImage(image, 
                               new Rectangle(0, 0, modifiedWidth, modifiedHeight),
                               new Rectangle(0, 0, originalWidth, originalHeight),
                               GraphicsUnit.Pixel);

            graphics.Dispose();
            return bitmap;

        }

        //---------------------------------------------------------------
        // Name : GetImageFormat
        // Abstract : Return image format
        //---------------------------------------------------------------
        private static ImageFormat GetImageFormat(string imageType)
        {
            ImageFormat imageFormat;
            switch (imageType)
            {
                case "image/jpg":
                    imageFormat = ImageFormat.Jpeg;
                    break;
                case "image/jpeg":
                    imageFormat = ImageFormat.Jpeg;
                    break;
                case "image/pjpeg":
                    imageFormat = ImageFormat.Jpeg;
                    break;
                case "image/gif":
                    imageFormat = ImageFormat.Gif;
                    break;
                case "image/png":
                    imageFormat = ImageFormat.Png;
                    break;
                case "image/x-png":
                    imageFormat = ImageFormat.Png;
                    break;
                default:
                    throw new Exception("Unsupported image type !");
            }

            return imageFormat;
        }

        //---------------------------------------------------------------
        // Name : IsImage
        // Abstract : Check if file is image
        //---------------------------------------------------------------
        private static bool IsImage(HttpPostedFileBase image)
        {
            if (image.ContentType.Contains("image"))
                return true;

            var formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };
            return formats.Any(x => image.FileName.EndsWith(x, StringComparison.OrdinalIgnoreCase));
        }

        //---------------------------------------------------------------
        // Name : ConvertImageToBytes
        // Abstract : Convert to byte array
        //---------------------------------------------------------------
        public static byte[] ConvertImageToBytes(this HttpPostedFileBase image)
        {
            byte[] abytImageArray = null;
            if (IsImage(image))
            {
                //var height = Int32.Parse(WebConfigurationManager.AppSettings["UploadImageHeight"]);
                //var width = Int32.Parse(WebConfigurationManager.AppSettings["UploadImageWidth"]);

                var height = DEFAULT_IMAGE_HEIGHT;
                var width = DEFAULT_IMAGE_WIDTH;
                var uploadImage = new Bitmap(image.InputStream);

                if (IsResizeNeeded(uploadImage, height, width))
                {
                    var resizedImage = ResizeBySize(uploadImage, height, width);
                    var resizedImageFormat = GetImageFormat(image.ContentType);

                    using (resizedImage)
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            if (resizedImageFormat != null)
                            {
                                resizedImage.Save(memoryStream, resizedImageFormat);
                            }
                            memoryStream.Position = 0;
                            abytImageArray = memoryStream.ToArray();
                        }
                    }

                }
                else
                {
                    abytImageArray = ByteArrayConversion(image);
                }
            }
            else
            {
                abytImageArray = ByteArrayConversion(image);
            }
            return abytImageArray;
        }

        //---------------------------------------------------------------
        // Name : ByteArrayConversion
        // Abstract : Convert to byte array
        //---------------------------------------------------------------
        private static byte[] ByteArrayConversion(HttpPostedFileBase image)
        {
            byte[] abytImageArray;
            var format = GetImageFormat(image.ContentType);
            var uploadImage = new Bitmap(image.InputStream);
        //    using (image.InputStream)
        //    {
                using (var memoryStream = new MemoryStream())
                {
                    //image.InputStream.CopyTo(memoryStream);
                    //abytImageArray = memoryStream.ToArray();
                    if (format != null)
                    {
                        uploadImage.Save(memoryStream, format);
                    }
                    memoryStream.Position = 0;
                    abytImageArray = memoryStream.ToArray();
                }
        //    }

           
            return abytImageArray;
        }

        public static Image ByteArrayToImage(byte[] byteArray)
        {
            using (var memoryStream = new MemoryStream())
            {
                Image image = Image.FromStream(memoryStream);
                return image;
            }
        }
    }
}