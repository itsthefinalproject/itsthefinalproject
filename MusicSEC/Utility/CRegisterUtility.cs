﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CRegisterUtility
// Abstract : Registration and Role Utility Functions
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicSEC.DAL;
using MusicSEC.Models;
using WebMatrix.WebData;
using System.Web.Security;

namespace MusicSEC.Utility
{
    public class CRegisterUtility
    {

        //---------------------------------------------------------------
        // Name : RegisterStudent
        // Abstract : Register a student and add role of potential student
        //---------------------------------------------------------------
        public int RegisterStudent(string username, string password)
        {
            try
            {
                WebSecurity.CreateUserAndAccount(username,password);
                if (!Roles.GetRolesForUser(username).Contains("PotentialStudent"))
                    Roles.AddUserToRole(username, "PotentialStudent");
                
            }
            catch (MembershipCreateUserException e)
            {
                // log error
            }

            return WebSecurity.GetUserId(username);
        }


        //---------------------------------------------------------------
        // Name : EnrollStudent
        // Abstract : Change a students role from potential to enrolled
        //---------------------------------------------------------------
        public bool EnrollStudent(CUnitOfWork unit, int studentId)
        {
            bool blnSuccess = true;
            try
            {
                int intUserId = unit.StudentRep.FindById(studentId).intUserID;
                if (intUserId != 0)
                {
                    string strUsername = unit.UsersRep.GetAll(x => x.UserId == intUserId).
                                                       Select(x => x.UserName).
                                                       FirstOrDefault();

                    if (Roles.GetRolesForUser(strUsername).Contains("PotentialStudent"))
                    {
                        Roles.RemoveUserFromRole(strUsername, "PotentialStudent");

                        if (!Roles.GetRolesForUser(strUsername).Contains("EnrolledStudent"))
                        {
                            Roles.AddUserToRole(strUsername, "EnrolledStudent");
                        }
                    }
                }
            }
            catch 
            {
                blnSuccess = false;
            }

            return blnSuccess;
        }
    }
}