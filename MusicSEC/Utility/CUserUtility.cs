﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CUserUtility
// Abstract : Utility functions to handle user related queries
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MusicSEC.DAL;

namespace MusicSEC.Utility
{
    public class CUserUtility
    {
        private static CUserUtility instance;

        // Singleton
        public static CUserUtility Instance
        {
            get { return instance ?? new CUserUtility(); }
        }

        //--------------------------------------------------------------
        // Name : GetCurrentInstructorId
        // Abstract : return current instructor id
        //--------------------------------------------------------------
        public int GetCurrentInstructorId(CUnitOfWork unit)
        {
            int intCurrentUserId = unit.CurrentUserID;
            int intCurrentInstructorId = unit.InstructorRep.GetAll(x => x.intUserID == intCurrentUserId).
                                                            Select(x => x.CInstructorID).
                                                            FirstOrDefault();

            return intCurrentInstructorId;
        }

        //--------------------------------------------------------------
        // Name : GetCurrentStudentId
        // Abstract : return current student id
        //--------------------------------------------------------------
        public int GetCurrentStudentId(CUnitOfWork unit)
        {
            int intCurrentUserId = unit.CurrentUserID;
            int intCurrentStudentId = unit.StudentRep.GetAll(x => x.intUserID == intCurrentUserId).
                                                            Select(x => x.CStudentID).
                                                            FirstOrDefault();
            return intCurrentStudentId;
        }

        
    }
}