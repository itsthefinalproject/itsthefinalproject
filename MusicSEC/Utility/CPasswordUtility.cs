﻿//---------------------------------------------------------------
// Author : Adam Shaver
// Class : CPasswordUtility
// Abstract : Utility functions for creating passwords
//---------------------------------------------------------------

//---------------------------------------------------------------
// Imports
//---------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace MusicSEC.Utility
{
    public class CPasswordUtility
    {
        private  Random random = new Random();
        private int intUserNameLength = 8;
        private int intPasswordLength = 8;
        private string strValidChars = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,0,1,2,3,4,5,6,7,8,9,$,#,%";
        
        // Default Constructor
        public CPasswordUtility()
        {
        
        }

        // Constructor with overloads
        public CPasswordUtility(int usernameLength, int passwordLength)
        {
            intUserNameLength = usernameLength;
            intPasswordLength = passwordLength;
        }

        //--------------------------------------------------------------
        // Name : CreateRandomUserName
        // Abstract : creates a random username
        //--------------------------------------------------------------
        public string CreateRandomUserName()
        {
            string strUserName = "";
            string[] astrChars = strValidChars.Split(',');

            for (int i = 0; i < intUserNameLength; i++)
            {
                try
                {
                    int intIndex = random.Next(0, astrChars.Length - 1);
                    strUserName += (i % intIndex == 0) ? astrChars[intIndex].ToLower() : astrChars[intIndex];
                }
                catch { }
            }

            return strUserName;
        }

        //--------------------------------------------------------------
        // Name : CreateRandomPassword
        // Abstract : create a random password
        //--------------------------------------------------------------
        public string CreateRandomPassword()
        {
            string strPassword = "";
            string[] astrChars = strValidChars.Split(',');

            for (int i = 0; i < intPasswordLength; i++)
            {
                int intIndex = random.Next(0, astrChars.Length - 1);
                strPassword += (i % (intIndex + 1) == 0) ? astrChars[intIndex].ToLower() : astrChars[intIndex];
            }
            return strPassword;
        }

    }
}